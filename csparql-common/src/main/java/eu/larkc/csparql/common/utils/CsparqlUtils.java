package eu.larkc.csparql.common.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.StringWriter;
import java.util.ArrayList;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.util.FileManager;

public class CsparqlUtils {
	public static String streamPath;
	public static final Long startStreamTime=1343860000000L;
	public static ArrayList<Integer> lengthOfWindows=new ArrayList<Integer>();
	public static final String streamFileName="stream.txt";
	public static String RDF2FusekiPath;
	public static String configPath;
	public static final String BKGChRFilePath="";
	public static final String followerBankFilePath="";
	public static final int streamLength=10000;
	
	public static String getFusekiRDFPath(){
		File[] files = new File(CsparqlUtils.configPath).listFiles();
		for (File file : files) {// for each stream and bkg.txt
			if (file.getName().contains("RDF_L")) {
				RDF2FusekiPath = file.getPath();
				break;
			}
		}
		return RDF2FusekiPath;
	}
	public static String getStreamTracePath(){
		File[] files = new File(CsparqlUtils.configPath).listFiles();
		for (File file : files) {// for each stream and bkg.txt
			if (file.getName().contains("stream_trace_")) {
				streamPath = file.getPath();
				break;
			}
		}
		return streamPath;
	}
	public static String serializeRDFFile(String filePath) throws Exception{
		File f = new File(filePath);
		Model m = ModelFactory.createDefaultModel();
		try{
			m.read(FileManager.get().open(f.getAbsolutePath()), null, "RDF/XML");
		} catch(Exception e){
			try{
				m.read(FileManager.get().open(f.getAbsolutePath()), null, "TURTLE");
			} catch(Exception e1){
				try{
					m.read(FileManager.get().open(f.getAbsolutePath()), null, "N-TRIPLE");
				} catch(Exception e2){
					m.read(FileManager.get().open(f.getAbsolutePath()), null, "RDF/JSON");
				}
			}
		}
		StringWriter sw = new StringWriter();
		m.write(sw);
		return sw.toString();
	}
	
	public static String serializeJenaModel(Model model) throws Exception{
		StringWriter sw = new StringWriter();
		model.write(sw);
		return sw.toString();
	}
	
	public static String fileToString(String filePath) throws Exception {
		File f = new File(filePath);
		BufferedReader br = new BufferedReader(new FileReader(f));
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();
			while (line != null) {
				sb.append(line);
				sb.append("\n");
				line = br.readLine();
			}
			return sb.toString();
		} finally {
			br.close();
		}
	}


}
