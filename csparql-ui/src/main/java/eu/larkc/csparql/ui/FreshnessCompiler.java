package eu.larkc.csparql.ui;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.jena.atlas.json.JsonObject;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.acqua.CacheAcqua;

import eu.larkc.csparql.common.RDFTable;
import eu.larkc.csparql.common.utils.CsparqlUtils;

public class FreshnessCompiler {
public static void main2(String args[]){
	JSONParser parser=new JSONParser();
	//HashSet<Var> keyvars = (HashSet<Var>) CacheAcqua.INSTANCE.getKeyVars();
	HashSet<Var> keyvars=new HashSet<Var>();
	keyvars.add(Var.alloc("S"));
	try{
		
		BufferedWriter wbmFreshness = new BufferedWriter(new FileWriter(new File("d:/mytest/wbmf400.txt")));//"../acquaCsparql/wbmFreshness.txt")));
		BufferedWriter cacheFreshness = new BufferedWriter(new FileWriter(new File("d:/mytest/randf400.txt")));//../acquaCsparql/cacheFreshness.txt")));
		
		JSONObject csparql =  (JSONObject)parser.parse(new FileReader("d:/mytest/csparql40016.json"));//./csparqlResult.json"));
		Iterator it = csparql.values().iterator();
		JSONArray csparqlArr = (JSONArray)it.next();
		
		JSONObject cache =  (JSONObject)parser.parse(new FileReader("d:/mytest/rand40016.json"));//"./cacheResult.json"));
		Iterator itcache = cache.values().iterator();
		JSONArray cacheArr = (JSONArray)itcache.next();
		
		JSONObject WBM =  (JSONObject)parser.parse(new FileReader("d:/mytest/wbm40016.json"));//"./WBMResult.json"));
		Iterator itWBM = WBM.values().iterator();
		JSONArray WBMArr = (JSONArray)itWBM.next();
		//ArrayList<JSONObject> WBMEvaluationResult=new ArrayList<JSONObject>();
		
		int csparqlResultCount=0;
			int staleResPerCacheEval=0;
			int staleResPerWBMEval=0;			
			int WBMResultCount=0;
			int cacheResultCount=0;
		// each evaluations
		for (int i=0;i<csparqlArr.size();i++)
		  {
			
			System.out.println(csparqlArr.size());
			ArrayList<JSONObject> cacheEvaluationResult=new ArrayList<JSONObject>();
			ArrayList<JSONObject> WBMEvaluationResult=new ArrayList<JSONObject>();			
			JSONObject csparqlEntry = (JSONObject) csparqlArr.get(i);
			JSONObject curCsparqlResults = (JSONObject)csparqlEntry.get("results");
			Iterator aCsparqlIt = curCsparqlResults.values().iterator();
			JSONArray curCsparqlRes = (JSONArray)aCsparqlIt.next();
			
			JSONObject cacheEntry = (JSONObject) cacheArr.get(i);
			JSONObject curCacheResults = (JSONObject)cacheEntry.get("results");
			Iterator aCacheIt = curCacheResults.values().iterator();
			JSONArray curCacheRes = (JSONArray)aCacheIt.next();
			for (int j=0;j<curCacheRes.size();j++){
		    	JSONObject oneRes = (JSONObject)curCacheRes.get(j);
		    	cacheEvaluationResult.add(oneRes);		    	
		    }
			
			JSONObject WBMEntry = (JSONObject) WBMArr.get(i);
			JSONObject curWBMResults = (JSONObject)WBMEntry.get("results");
			Iterator aWBMIt = curWBMResults.values().iterator();
			JSONArray curWBMRes = (JSONArray)aWBMIt.next();
			for (int j=0;j<curWBMRes.size();j++){
		    	JSONObject oneRes = (JSONObject)curWBMRes.get(j);
		    	WBMEvaluationResult.add(oneRes);		    	
		    }
			
			//for each individual response per evaluation
		    for (int j=0;j<curCsparqlRes.size();j++){
		    	JSONObject oneRes = (JSONObject)curCsparqlRes.get(j);
		    	csparqlResultCount++;
		    	// for testing purposes to test if the window content of cache and csparql were equal in this evaluation
		    	boolean exists = true;
		    	for(int l=0;l<cacheEvaluationResult.size();l++){
		    		exists=true;
		    		JSONObject tempCache=cacheEvaluationResult.get(l);
		    		Iterator<Var> keyIt = keyvars.iterator();
		    		while(keyIt.hasNext()){
		    			Var tempKey= keyIt.next();
		    			Object freshValueOfTmpeKeyVar = oneRes.get(tempKey.getVarName().toString());
		    			Object cachedValueOfTmpeKeyVar = tempCache.get(tempKey.getVarName().toString());
		    			if(!freshValueOfTmpeKeyVar.equals(cachedValueOfTmpeKeyVar))
		    				{
		    					exists=false;
		    					break;
		    				}
		    		}
		    		if(exists) break;
		    	}
		    	if(exists=false) System.out.println("no matching key for csparql result "+oneRes+ "in cache window because window content for this evaluation in csparql and cache were different ... ");
		    	///////////////////////
		    	// for testing purposes to test if the window content of WBM and csparql were equal in this evaluation
		    	for(int l=0;l<WBMEvaluationResult.size();l++){
		    		exists=true;
		    		JSONObject tempCache=WBMEvaluationResult.get(l);
		    		Iterator<Var> keyIt = keyvars.iterator();
		    		while(keyIt.hasNext()){
		    			Var tempKey= keyIt.next();
		    			Object freshValueOfTmpeKeyVar = oneRes.get(tempKey.getVarName().toString());
		    			Object WBMValueOfTmpeKeyVar = tempCache.get(tempKey.getVarName().toString());
		    			if(!freshValueOfTmpeKeyVar.equals(WBMValueOfTmpeKeyVar))
		    				{
		    					exists=false;
		    					break;
		    				}
		    		}
		    		if(exists) break;
		    	}
		    	if(exists=false) System.out.println("no matching key for csparql result "+oneRes+ "in WBM window because window content for this evaluation in csparql and cache were different ... ");
		    	///////////////////////
		    	if(!cacheEvaluationResult.contains(oneRes))
		    		staleResPerCacheEval++;
		    	if(!WBMEvaluationResult.contains(oneRes))
		    		staleResPerWBMEval++;		    	
		    }
		    cacheResultCount+=cacheEvaluationResult.size();
	    	WBMResultCount+=WBMEvaluationResult.size();
	    	wbmFreshness.write( (1-(double)staleResPerWBMEval/(double)csparqlResultCount)+"\n");
		    cacheFreshness.write((1-(double)staleResPerCacheEval/(double)csparqlResultCount) +"\n");
		    //System.out.println("csparql cummulative completeness "+ ((double)csparqlResultCount/(double)cacheResultCount));
		    //System.out.println("WBM cummulative completeness "+ ((double)WBMResultCount/(double)cacheResultCount));
		  }
		wbmFreshness.flush();
		wbmFreshness.close();
		cacheFreshness.flush();
		cacheFreshness.close();
	}catch(Exception e){e.printStackTrace();}
}
public static void main(String args[]){
	try{
		JSONParser parser=new JSONParser();
	
	RDFTable nul=new RDFTable();
	nul.setJsonSerialization("{\"responses\":[{  \"head\": {    \"vars\": [ \"S\" , \"P\" , \"O\" , \"P2\" , \"O2\" ]  } ,  \"results\": {    \"bindings\": []}}]}");
	JSONObject WBM=null;
	WBM = (JSONObject) parser
			.parse(nul.getJsonSerialization());
	//Iterator itWBM = WBM.values().iterator();
	//WBMArrList.add((JSONArray) itWBM.next());
	JSONArray WBMArr = (JSONArray)WBM.values().iterator().next();
	JSONObject WBMEntry = (JSONObject) WBMArr.get(0);
	JSONObject curWBMResults = (JSONObject)WBMEntry.get("results");
	
	Iterator aWBMIt = curWBMResults.values().iterator();
	JSONArray curWBMRes = (JSONArray)aWBMIt.next();
	System.out.println(">>>"+curWBMRes.size());
	
	for (int j=0;j<curWBMRes.size();j++){
    	JSONObject oneRes = (JSONObject)curWBMRes.get(j);
    	//WBMEvaluationResult.add(oneRes);		    	
    }
	
	}catch(Exception e){e.printStackTrace();}
}
}
