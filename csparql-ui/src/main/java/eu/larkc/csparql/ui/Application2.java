/*******************************************************************************
 * Copyright 2014 DEIB -Politecnico di Milano
 *   
 *  Marco Balduini (marco.balduini@polimi.it)
 *  Emanuele Della Valle (emanuele.dellavalle@polimi.it)
 *  Davide Barbieri
 *   
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *   
 *  	http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *   
 *  Acknowledgements:
 *  
 *  This work was partially supported by the European project LarKC (FP7-215535)
 ******************************************************************************/
package eu.larkc.csparql.ui;

import java.awt.List;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;

import org.openrdf.model.URI;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
//import org.openrdf.sail.nativerdf.NativeStore;

import com.hp.hpl.jena.query.DatasetAccessor;
import com.hp.hpl.jena.query.DatasetAccessorFactory;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.sparql.engine.acqua.CacheAcqua;
import com.hp.hpl.jena.sparql.engine.main.iterator.QueryIterServiceWBM;
import com.hp.hpl.jena.update.UpdateExecutionFactory;
import com.hp.hpl.jena.update.UpdateFactory;
import com.hp.hpl.jena.update.UpdateProcessor;
import com.hp.hpl.jena.update.UpdateRequest;

import eu.larkc.csparql.cep.api.OracleTestGenerator;
import eu.larkc.csparql.cep.api.RDFStreamAggregationTestGenerator;
import eu.larkc.csparql.cep.api.RdfQuadruple;
import eu.larkc.csparql.cep.api.RdfStream;
import eu.larkc.csparql.cep.api.TestGenFromStreamFile;
import eu.larkc.csparql.cep.api.TestGenerator;
import eu.larkc.csparql.common.utils.CsparqlUtils;
import eu.larkc.csparql.core.Configuration;
import eu.larkc.csparql.core.engine.CsparqlEngine;
import eu.larkc.csparql.core.engine.CsparqlEngineImpl;
import eu.larkc.csparql.core.engine.CsparqlQueryResultProxy;
import eu.larkc.csparql.core.old_parser.CSparqlParser.string_return;

public final class Application2 {

	/**
	 * @param args
	 */
	public static BufferedWriter resultWriter = null;
	public static BufferedWriter stats = null;
	public static BufferedWriter burnBW=null;
	public static String modeOfExecution ;


	public static void restartFuseki() {
		try {
			String UQ = "DELETE    { ?a ?b ?c } where{?a ?b ?c}; ";
			UpdateRequest query = UpdateFactory.create(UQ);
			UpdateProcessor qexec = UpdateExecutionFactory.createRemoteForm(
					query, "http://localhost:3030/test/update");
			qexec.execute();
			String serviceURI = "http://localhost:3030/test/data";
			DatasetAccessor accessor;
			accessor = DatasetAccessorFactory.createHTTP(serviceURI);
			Model model = ModelFactory.createDefaultModel();
			model.read(new FileInputStream("../acquaCsparql/test.ttl"), null,
					"TTL");
			accessor.putModel(model);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main2(final String[] args) {

		try {
			System.setOut(new PrintStream(new BufferedOutputStream(
					new FileOutputStream("../acquaCsparql/output"
							+ modeOfExecution + ".txt"))));
			resultWriter = new BufferedWriter(new FileWriter(new File("./"
					+ modeOfExecution + "Result.json")));
			stats = new BufferedWriter(new FileWriter(new File(
					"../acquaCsparql/runstats" + modeOfExecution + ".txt")));
			stats.write("actualLengthOfWindow, TimePerExec,timeConst,timeNext,totalMemoryPerExec ,wbmCallCount, csparqlCallCount,aSubsetOfWindowCatchedForResult,curWinContent,timeTakenForCurEval \n");

		} catch (Exception e) {
			System.out.println("can't initiate result file");
			e.printStackTrace();
		}
		restartFuseki();
		// final String queryGetAll =
		// "REGISTER QUERY PIPPO AS SELECT ?S ?P ?O FROM STREAM <http://www.larkc.eu/defaultRDFInputStream> [RANGE 3s STEP 1s] WHERE { ?S ?P ?O }";
		final String queryGetAll = "REGISTER QUERY PIPPO AS SELECT * FROM STREAM <http://www.larkc.eu/defaultRDFInputStream> [RANGE 6s STEP 2s]   WHERE { ?S ?P ?O SERVICE <http://localhost:3030/test/sparql> {?S ?P2 ?O2}}";

		final String queryGetEverythingFromBothStream = "REGISTER QUERY PIPPO AS SELECT ?S ?P ?O FROM STREAM <http://www.glue.com/stream> [RANGE TRIPLES 1] FROM STREAM <http://myexample.org/stream> [RANGE TRIPLES 1] WHERE { ?S ?P ?O }";

		final String queryAnonymousNodes = "REGISTER QUERY PIPPO AS CONSTRUCT {                        [] <http://ex.org/by> ?s  ;  <http://ex.org/count> ?n . } FROM STREAM <http://www.larkc.eu/defaultRDFInputStream> [RANGE TRIPLES 10]                        WHERE {                                { SELECT ?s ?p (count(?o) as ?n)                                  WHERE { ?s ?p ?o }                                  GROUP BY ?s }                              }";

		final String queryNoCount = "REGISTER QUERY PIPPO AS "
				+ "SELECT ?p "
				+ " FROM STREAM <http://myexample.org/stream> [RANGE TRIPLES 1] "
				+ " FROM <http://dbpedia.org/resource/Castello_Sforzesco> "
				+ "WHERE { ?s ?p ?o }";

		final String queryCount = "REGISTER QUERY PIPPO AS "
				+ "SELECT ?t (count(?t) AS ?conto)"
				+ " FROM STREAM <http://www.glue.com/stream> [RANGE TRIPLES 30] WHERE { ?s <http://rdfs.org/sioc/ns#topic> ?t } "
				+ "GROUP BY ?t ";

		final String querySimpleCount = "REGISTER QUERY PIPPO AS "
				+ "SELECT ?s (COUNT(?s) AS ?conto) FROM STREAM <http://www.glue.com/stream> [RANGE TRIPLES 1] WHERE { ?s ?p ?o } GROUP BY ?s";

		final String queryGetKB = "REGISTER QUERY PIPPO AS "
				+ "SELECT ?s ?p ?o FROM <http://rdfs.org/sioc/ns>\n FROM STREAM <http://www.glue.com/stream> [RANGE TRIPLES 1] WHERE { ?s ?p ?o }";

		final String queryGetAll2 = "REGISTER QUERY PIPPO AS "
				+ "CONSTRUCT { <http://www.streams.org/s> <http://www.streams.org/s> ?n }"
				+ " FROM STREAM <http://myexample.org/stream> [RANGE TRIPLES 2] "
				+ " WHERE {" + "  { SELECT (count(?o) as ?n) "
				+ "  { ?s ?p ?o }" + "   GROUP BY ?p } " + "} ";
		final CsparqlEngine engine = new CsparqlEngineImpl(resultWriter, stats,

				modeOfExecution);
		engine.initialize();

		/*
		 * ArrayList<BindingSet> bindList; RepositoryConnection conn=null; long
		 * sleep = 1000;
		 * 
		 * bindList = new ArrayList<BindingSet>();
		 * 
		 * String baseURI = "http://www.streamreasoning.org/schema/benchmark#";
		 * 
		 * URI graphList = new URIImpl(baseURI+"graphsList"); URI hasTimestamp =
		 * new URIImpl(baseURI+"hasTimestamp");
		 * 
		 * Repository repo = new SailRepository(new
		 * NativeStore(Configuration.getRepoDir(), "cspo,cops"));
		 * 
		 * try { repo.initialize(); } catch (RepositoryException e) {
		 * e.printStackTrace(); }
		 * 
		 * try { conn=repo.getConnection(); String qg="SELECT ?g " + "FROM <" +
		 * graphList + "> " + "WHERE{" + "?g <" + hasTimestamp +
		 * "> ?timestamp. " + "}"; TupleQuery
		 * q=conn.prepareTupleQuery(QueryLanguage.SPARQL, qg); TupleQueryResult
		 * tqr=q.evaluate(); while (tqr.hasNext()){ bindList.add(tqr.next()); }
		 * } catch (RepositoryException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } catch (MalformedQueryException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); } catch
		 * (QueryEvaluationException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } OracleTestGenerator tg = new
		 * OracleTestGenerator("http://ex.org/streams/test", 1, conn,bindList,
		 * sleep);
		 */

		// final RDFStreamAggregationTestGenerator tg = new
		// RDFStreamAggregationTestGenerator("http://www.larkc.eu/defaultRDFInputStream");
		// final GlueStreamGenerator tg = new GlueStreamGenerator();
		// TestGenerator tg1 = new
		// TestGenerator("http://www.larkc.eu/defaultRDFInputStream",resultWriter);

		// engine.registerStream(tg2);
		/*
		 * final Thread t = new Thread(tg1); t.start();
		 */

		CsparqlQueryResultProxy c1 = null;

		final CsparqlQueryResultProxy c2 = null;

		try {
			CacheAcqua.INSTANCE.readCRandFB();
			/*
			 * Thread t1 = new Thread(new Runnable() { public void run() {
			 * CacheAcqua.INSTANCE.serverUpdater(); } }); t1.start();
			 */
			System.out.println("???????????????" + stats);
			TestGenFromStreamFile tg1 = new TestGenFromStreamFile(
					"http://www.larkc.eu/defaultRDFInputStream", resultWriter,
					stats, modeOfExecution);
			final Thread t = new Thread((Runnable) tg1);

			// RdfStream rs = engine.registerStream(tg);
			// engine.unregisterStream(rs.getIRI());
			engine.registerStream(tg1);
			c1 = engine.registerQuery(queryGetAll, false);
			// tg1.put(new RdfQuadruple("http://myexample.org/S1",
			// "http://myexample.org/P1", "http://myexample.org/O1",
			// System.currentTimeMillis()));
			// long now = System.currentTimeMillis();
			// Thread.sleep((now / 5000 + 1) * 5000 - now);
			// TODO shen what is this?
			// tg1.put(new RdfQuadruple("http://myexample.org/S1",
			// "http://myexample.org/P1", "http://myexample.org/O1",
			// System.currentTimeMillis()));
			// Thread.sleep(5000);//to get the cache initialized

			t.start();
		} catch (final Exception ex) {
			System.out.println("errore di parsing: " + ex.getMessage());
		}
		if (c1 != null) {
			c1.addObserver(new TextualFormatter());
		}
	}

/*	public static void main(final String[] args) {

		ArrayList<String> modes = new ArrayList<String>();
		modes.add("csparql");
		modes.add("cache");
		modes.add("rand");
		modes.add("WBM");
		modes.add("SBMAgg");
		modes.add("SBMBGP");
		modes.add("csparql");
		
		File[] files = new File(
				"D:/acqua_dev/newCSPARQL/CSPARQL-engine/acquaCsparql/tests")
				.listFiles();
		for (File file : files) {//for each stream and bkg.txt
			if (file.isDirectory()) {
				CsparqlUtils.configPath=file.getAbsolutePath()+file.separatorChar;				
				System.out.println(CsparqlUtils.configPath);
				int m = 0;
				while (true) {
					// System.out.println(">>>>>>>>>>>"+CsparqlUtils.running);
					if (!CsparqlUtils.running) {
						if (m < 7) {
							modeOfExecution = modes.get(m);
							if (modeOfExecution.equalsIgnoreCase("WBM")||modeOfExecution.equalsIgnoreCase("rand")) {
								//
								int u = 0;
								while (true) {
									System.out.println(u);
									if (!CsparqlUtils.running) {
										if (u < 1001) {
											System.out.println("statring "+ modeOfExecution+" "
													+ u);
											CsparqlUtils.running = true;
											QueryIterServiceWBM.burnRate.clear();
											try {
												stats = new BufferedWriter(
														new FileWriter(
																new File(
																		CsparqlUtils.configPath
																				+ "runstats"
																				+ modeOfExecution
																				+ "_"
																				+ u
																				+ ".txt")));
												stats.write("actualLengthOfWindow, TimePerExec,timeConst,timeNext,  totalMemoryPerExec ,randCallCount, wbmCallCount, csparqlCallCount,aSubsetOfWindowCatchedForResult,curWinContent,timeTakenForCurEval \n");
												resultWriter = new BufferedWriter(
														new FileWriter(
																new File(
																		CsparqlUtils.configPath
																				+ modeOfExecution
																				+ "_"
																				+ u
																				+ "Result.json")));
												
												burnBW = new BufferedWriter(
														new FileWriter(
																new File(
																		CsparqlUtils.configPath
																				+ modeOfExecution
																				+ "_"
																				+ u
																				+ "burnstats.txt")));

												} catch (Exception e) {
												System.out
														.println("can't initiate result file");
												e.printStackTrace();
											}
											restartFuseki();
											final String queryGetAll = "REGISTER QUERY PIPPO AS SELECT * FROM STREAM <http://www.larkc.eu/defaultRDFInputStream> [RANGE 6s STEP 5s]   WHERE { ?S ?P ?O SERVICE <http://localhost:3030/test/sparql> {?S ?P2 ?O2}}";
											final CsparqlEngine engine = new CsparqlEngineImpl(
													resultWriter, stats,
													modeOfExecution);
											engine.initialize();
											engine.setMOE(modeOfExecution);
											engine.setUpdateBudget(u);
											CsparqlQueryResultProxy c1 = null;
											try {
												TestGenFromStreamFile tg1 = new TestGenFromStreamFile(
														"http://www.larkc.eu/defaultRDFInputStream",
														resultWriter, stats,
														modeOfExecution);
												final Thread t = new Thread(
														(Runnable) tg1);
												engine.registerStream(tg1);
												c1 = engine.registerQuery(
														queryGetAll, false);
												CacheAcqua.INSTANCE.readCRandFB();											
												t.start();
												t.join();
												if(modeOfExecution.equalsIgnoreCase("WBM")){
												Iterator<Integer> kit = QueryIterServiceWBM.burnRate.iterator();
												while (kit.hasNext()) {
													Integer temp = kit.next();
													// System.out.println(">>>>>>>>."+temp.get(Var.alloc("S")).toString());
													burnBW.write(temp.toString() + "\n");
												}
												burnBW.flush();
												burnBW.close();	}										
											} catch (final Exception ex) {
												System.out
														.println("errore di parsing: "
																+ ex.getMessage());
											}
											if (c1 != null) {
												c1.addObserver(new TextualFormatter());
											}
											if(u<100)u += 20;
											else if(u<500) u+=100;
											else if (u<1000) u+=200;
										} else
											break;
									}
								}
							} else if (modeOfExecution.equalsIgnoreCase("cache")||modeOfExecution.equalsIgnoreCase("csparql")) {
								System.out.println("statring "
										+ modeOfExecution);
								CsparqlUtils.running = true;
								try {
									// System.setOut(new PrintStream(new
									// BufferedOutputStream(new
									// FileOutputStream("../acquaCsparql/output"+
									// modeOfExecution + ".txt"))));
									stats = new BufferedWriter(
											new FileWriter(new File(
													CsparqlUtils.configPath
															+ "runstats"
															+ modeOfExecution
															+ ".txt")));
									stats.write("actualLengthOfWindow, TimePerExec,timeConst,timeNext,  totalMemoryPerExec ,wbmCallCount,randCallCount, csparqlCallCount,aSubsetOfWindowCatchedForResult,curWinContent,timeTakenForCurEval \n");
									resultWriter = new BufferedWriter(
											new FileWriter(new File(
													CsparqlUtils.configPath
															+ modeOfExecution
															+ "Result.json")));
								} catch (Exception e) {
									System.out
											.println("can't initiate result file");
									e.printStackTrace();
								}
								restartFuseki();
								final String queryGetAll = "REGISTER QUERY PIPPO AS SELECT * FROM STREAM <http://www.larkc.eu/defaultRDFInputStream> [RANGE 6s STEP 5s]   WHERE { ?S ?P ?O SERVICE <http://localhost:3030/test/sparql> {?S ?P2 ?O2}}";
								final CsparqlEngine engine = new CsparqlEngineImpl(
										resultWriter, stats, modeOfExecution);
								engine.initialize();
								engine.setMOE(modeOfExecution);
								CsparqlQueryResultProxy c1 = null;
								try {
									// CacheAcqua.INSTANCE.readCRandFB(); //in
									// csparql
									// and cache we don't need reading cr and fb
									TestGenFromStreamFile tg1 = new TestGenFromStreamFile(
											"http://www.larkc.eu/defaultRDFInputStream",
											resultWriter, stats,
											modeOfExecution);

									final Thread t = new Thread((Runnable) tg1);
									engine.registerStream(tg1);
									c1 = engine.registerQuery(queryGetAll,
											false);
									CacheAcqua.INSTANCE.readCRandFB();								
									t.start();
									t.join();
								} catch (final Exception ex) {
									System.out.println("errore di parsing: "
											+ ex.getMessage());
								}
								if (c1 != null) {
									c1.addObserver(new TextualFormatter());
								}

							}
							//else if (){
								//how to run SBMAGG or SBMBGP
							//}
							m++;
						} else
							break;
					}
				}
			}
		}
	}*/

	private Application2() {
		// hidden constructor
	}

}
