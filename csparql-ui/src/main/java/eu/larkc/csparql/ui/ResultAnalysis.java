package eu.larkc.csparql.ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class ResultAnalysis {
	
	public TreeMap<Long, TreeSet<String>> getResultSet(String csparqlDir) throws NumberFormatException, IOException {
		TreeMap<Long, TreeSet<String>> csparqlResultSet = new TreeMap<Long, TreeSet<String>>();
		BufferedReader csparqlResult = new BufferedReader(new FileReader(new File(csparqlDir)));
		String csparqlResultLine = null;
		long lastWindowTimeStamp = 0;
		while ((csparqlResultLine = csparqlResult.readLine()) != null) {
			String[] linebreak = csparqlResultLine.split("\t");
			long tempWindowTimeStamp = Long.parseLong(linebreak[0]);
			if (lastWindowTimeStamp != tempWindowTimeStamp) {
				TreeSet<String> tempValue = new TreeSet<String>();
				tempValue.add(csparqlResultLine);
				csparqlResultSet.put(tempWindowTimeStamp, tempValue);
				lastWindowTimeStamp = tempWindowTimeStamp;
			} else {
				Set<String> tempValue = csparqlResultSet.get(tempWindowTimeStamp);
				tempValue.add(csparqlResultLine);
			}

		}
		csparqlResult.close();
		return csparqlResultSet;
	}
	
	/*public void compareTwoSetMap(TreeMap<Long, TreeSet<String>> setMapNew, TreeMap<Long, TreeSet<String>> setMapBase) {

		for (Entry<Long, TreeSet<String>> e : setMapNew.entrySet()) {

			TreeSet<String> tempNew = e.getValue();
			TreeSet<String> tempBase = setMapBase.get(e.getKey());

			if (tempNew == null || tempBase == null || tempBase.size() != tempNew.size()) {
				System.out.println("here");
				throw new RuntimeException("Result set size is not equal");
			}
			int freshResults = 0;
			int stallResults = 0;
			for (String sNew : tempNew) {
				if (tempBase.contains(sNew)) {
					freshResults++;
				} else {
					stallResults++;
				}
			}

			StatUtilits.INSTANCE.getCounter("resultFreshResults").addN(freshResults);
			StatUtilits.INSTANCE.getCounter("resultStallResults").addN(stallResults);
			StatUtilits.INSTANCE.getCounter("resultTotalResults").addN(tempNew.size());
			StatUtilits.INSTANCE.getCounterDouble("resultFreshResultsPrecent").addN((double) freshResults / (double) tempNew.size());

		}

	}
	
	public void performAnalysis(String inputFolderTopLevel, String fileName, String comparingTo) throws NumberFormatException, IOException {

		String csparqlDir = this.getCsparqlPath();
		inputFile = fileName;

		TreeMap<Long, TreeSet<String>> csparqlResetSet = getResultSet(csparqlDir);
		TreeMap<Long, TreeSet<String>> tempResultSet = getResultSet(fileName);
		// compare two results set
		compareTwoSetMap(tempResultSet, csparqlResetSet);
		printCountersAndReset();

	}*/
}
