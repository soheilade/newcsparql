package eu.larkc.csparql.ui;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.hp.hpl.jena.sparql.core.Var;

import eu.larkc.csparql.common.utils.CsparqlUtils;

public class FullFreshnessCompiler {
	public static void main(String args[]) {
		int maxb=1001;
		JSONParser parser = new JSONParser();
		// HashSet<Var> keyvars = (HashSet<Var>)
		// CacheAcqua.INSTANCE.getKeyVars();
		HashSet<Var> keyvars = new HashSet<Var>();
		keyvars.add(Var.alloc("S"));
		try {
			File[] files = new File(
					//"D:/acqua_dev/newCSPARQL/CSPARQL-engine/acquaCsparql/tests")
					"/home/soheila/git/newcsparql/acquaCsparql/Test1000/")
					.listFiles();
			for (File file : files) {// for each stream and bkg.txt
				if (file.isDirectory()) {
					//create freshness files for cache and all wbm and all rands
					System.out.println("hello");
					CsparqlUtils.configPath = file.getAbsolutePath()
							+ file.separatorChar;
					System.out.println(CsparqlUtils.configPath);
					ArrayList<BufferedWriter> wbmFreshness = new ArrayList<BufferedWriter>();
					ArrayList<BufferedWriter> randFreshness = new ArrayList<BufferedWriter>();
					ArrayList<BufferedWriter> globalFreshness = new ArrayList<BufferedWriter>();
					for (int u = 0; u < maxb; ) {
						
						wbmFreshness.add(new BufferedWriter(new FileWriter(
								new File(CsparqlUtils.configPath
										+ "wbmFreshness" + u + ".txt"))));
						globalFreshness.add(new BufferedWriter(new FileWriter(
								new File(CsparqlUtils.configPath
										+ "globalFreshness" + u + ".txt"))));
						randFreshness.add(new BufferedWriter(new FileWriter(
								new File(CsparqlUtils.configPath
										+ "randFreshness" + u + ".txt"))));
						if(u<100)u += 20;
						else if(u<500) u+=100;
						else if (u<1000) u+=200;
						}
					BufferedWriter cacheFreshness = new BufferedWriter(
							new FileWriter(new File(CsparqlUtils.configPath
									+ "cacheFreshness.txt")));
					
					//reader of result files for cache and csparql
					JSONObject csparql = (JSONObject) parser
							.parse(new FileReader(CsparqlUtils.configPath
									+ "csparqlResult.json"));
					Iterator it = csparql.values().iterator();
					JSONArray csparqlArr = (JSONArray) it.next();

					JSONObject cache = (JSONObject) parser
							.parse(new FileReader(CsparqlUtils.configPath
									+ "cacheResult.json"));
					Iterator itcache = cache.values().iterator();
					JSONArray cacheArr = (JSONArray) itcache.next();
					//readers of result files wbm0-1000 rand0-1000
					ArrayList<JSONArray> WBMArrList = new ArrayList<JSONArray>();
					ArrayList<JSONArray> globalArrList = new ArrayList<JSONArray>();					
					ArrayList<JSONArray> randArrList = new ArrayList<JSONArray>();
					for (int u = 0; u < maxb;) {
						
						JSONObject WBM=null;
						WBM = (JSONObject) parser
								.parse(new FileReader(CsparqlUtils.configPath
										+ "WBM_" + u + "Result.json"));
						Iterator itWBM = WBM.values().iterator();
						WBMArrList.add((JSONArray) itWBM.next());
						
						
						JSONObject global=null;
						global = (JSONObject) parser
								.parse(new FileReader(CsparqlUtils.configPath
										+ "global_" + u + "Result.json"));
						Iterator itglobal = global.values().iterator();
						globalArrList.add((JSONArray) itglobal.next());
						
						JSONObject rand = null;
						try{rand=(JSONObject) parser
								.parse(new FileReader(CsparqlUtils.configPath
										+ "rand_" + u + "Result.json"));
						Iterator itrand = rand.values().iterator();
						randArrList.add((JSONArray) itrand.next());
						}catch(Exception e){
							 System.out.println("couldn't read the result file");
							 randArrList.add(null);}
						if(u<100)u += 20;
						else if(u<500) u+=100;
						else if (u<1000) u+=200;
					}
					// each evaluations we get the list of current elements in
					// the
					// window
					for (int i = 0; i < csparqlArr.size(); i++) {//per window
						int csparqlResultCount = 0;
						int staleResPerCacheEval = 0;
						ArrayList<Integer> staleResPerWBMEval = new ArrayList<Integer>();
						ArrayList<Integer> staleResPerglobalEval = new ArrayList<Integer>();						
						ArrayList<Integer> staleResPerrandEval = new ArrayList<Integer>();
						
						int cacheResultCount = 0;
						
						staleResPerWBMEval.clear();
						staleResPerglobalEval.clear();
						staleResPerrandEval.clear();
						for (int u = 0; u < maxb; ) {
						
							staleResPerWBMEval.add(0);
							staleResPerglobalEval.add(0);
							staleResPerrandEval.add(0);
							if(u<100)u += 20;
							else if(u<500) u+=100;
							else if (u<1000) u+=200;
						}
						ArrayList<JSONObject> cacheEvaluationResult = new ArrayList<JSONObject>();
						ArrayList<ArrayList<JSONObject>> WBMEvaluationResultArray = new ArrayList<ArrayList<JSONObject>>();
						ArrayList<ArrayList<JSONObject>> globalEvaluationResultArray = new ArrayList<ArrayList<JSONObject>>();
						ArrayList<ArrayList<JSONObject>> randEvaluationResultArray = new ArrayList<ArrayList<JSONObject>>();
						JSONObject csparqlEntry = (JSONObject) csparqlArr
								.get(i);// ith window
						JSONObject curCsparqlResults = (JSONObject) csparqlEntry
								.get("results");
						Iterator aCsparqlIt = curCsparqlResults.values()
								.iterator();
						JSONArray curCsparqlRes = (JSONArray) aCsparqlIt.next();// ith winodow
						//fill the result of that window for cache 
						
						JSONObject cacheEntry = (JSONObject) cacheArr.get(i);
						JSONObject curCacheResults = (JSONObject) cacheEntry
								.get("results");
						Iterator aCacheIt = curCacheResults.values().iterator();
						JSONArray curCacheRes = (JSONArray) aCacheIt.next();// ith window
						for (int j = 0; j < curCacheRes.size(); j++) {// all
																		// window
																		// entries
																		// of
																		// current
																		// ith
																		// eval
																		// in
																		// cache
							JSONObject oneRes = (JSONObject) curCacheRes.get(j);
							cacheEvaluationResult.add(oneRes);
						}
						//fill the result of that window(ith window) for wbm 0-1000 , rand 0-1000
						for (int u = 0; u < maxb; ) {
							int index=0;
							if(u<=100) index=u/20;
							else if (u<=500) index=(u-100)/100+5;
							else if (u<1000) index=(u-500)/200+9;
							
							WBMEvaluationResultArray
									.add(new ArrayList<JSONObject>());
							 
							JSONObject wbmEntry = (JSONObject) WBMArrList.get(index).get(i);// i th entry with u=x
							JSONObject curWBMResults = (JSONObject) wbmEntry
									.get("results");
							Iterator aWBMIt = curWBMResults.values().iterator();
							JSONArray curWBMRes = (JSONArray) aWBMIt.next();
							for (int j = 0; j < curWBMRes.size(); j++) {
								JSONObject oneRes = (JSONObject) curWBMRes
										.get(j);
								//System.out.println("wbm oneres"+oneRes);
								WBMEvaluationResultArray.get(index).add(oneRes);// all window entries of
														// current ith eval in
														// wbm with u=x
							}
							if(curWBMRes.size()==0) {System.out.println("result of window is empty because update budget is higher");WBMEvaluationResultArray.get(index).add(null);}
//-------------------------------------------
							
							globalEvaluationResultArray
							.add(new ArrayList<JSONObject>());
					 
					JSONObject globalEntry = (JSONObject) globalArrList.get(index).get(i);// i th entry with u=x
					JSONObject curglobalResults = (JSONObject) globalEntry
							.get("results");
					Iterator aglobalIt = curglobalResults.values().iterator();
					JSONArray curglobalRes = (JSONArray) aglobalIt.next();
					for (int j = 0; j < curglobalRes.size(); j++) {
						JSONObject oneRes = (JSONObject) curglobalRes
								.get(j);
						//System.out.println("wbm oneres"+oneRes);
						globalEvaluationResultArray.get(index).add(oneRes);// all window entries of
												// current ith eval in
												// wbm with u=x
					}
					if(curglobalRes.size()==0) {System.out.println("result of window is empty because update budget is higher");globalEvaluationResultArray.get(index).add(null);}
					//-------------------------------------------
					randEvaluationResultArray.add(new ArrayList<JSONObject>());
							JSONObject randEntry = (JSONObject) randArrList.get(index).get(i);// i th entry with u=x
					JSONObject currandResults = (JSONObject) randEntry
							.get("results");
					Iterator arandIt = currandResults.values().iterator();
					JSONArray currandRes = (JSONArray) arandIt.next();
					if(u>300) System.out.println(currandRes+" "+currandRes.size());
					for (int j = 0; j < currandRes.size(); j++) {
						JSONObject oneRes = (JSONObject) currandRes
								.get(j);
						randEvaluationResultArray.get(index).add(oneRes);// all window entries of current ith eval in wbm with u=x
					}
					if(currandRes.size()==0) {System.out.println("result of window is empty because update budget is higher");randEvaluationResultArray.get(index).add(null);}//ith window with u=x
					if(u<100)u += 20;
					else if(u<500) u+=100;
					else if (u<1000) u+=200;
					
						}
						// for each individual response per cspraql window we look it up in the corresponding window in cache , wbm 0-1000 and rand 0-1000
						for (int j = 0; j < curCsparqlRes.size(); j++) {
							JSONObject oneRes = (JSONObject) curCsparqlRes.get(j);
							//System.out.println(">>>>>>>>"+oneRes);
							csparqlResultCount++;
							// for testing purposes to test if the window
							// content of
							// cache and csparql were equal in this evaluation
							boolean exists = true;
							for (int l = 0; l < cacheEvaluationResult.size(); l++) {
								exists = true;
								JSONObject tempCache = cacheEvaluationResult
										.get(l);
								Iterator<Var> keyIt = keyvars.iterator();
								while (keyIt.hasNext()) {
									Var tempKey = keyIt.next();
									Object freshValueOfTmpeKeyVar = oneRes
											.get(tempKey.getVarName()
													.toString());
									Object cachedValueOfTmpeKeyVar = tempCache
											.get(tempKey.getVarName()
													.toString());
									if (!freshValueOfTmpeKeyVar
											.equals(cachedValueOfTmpeKeyVar)) {
										exists = false;
										break;
									}
								}
								if (exists)
									break;
							}
							if (exists = false)
								System.out
										.println("no matching key for csparql result "
												+ oneRes
												+ "in cache window because window content for this evaluation in csparql and cache were different ... ");
							if (!cacheEvaluationResult.contains(oneRes))
								staleResPerCacheEval++;
							// ///////////////wbm
							for (int u = 0; u < maxb; ) {// computing freshness for each u
								int index=0;
								if(u<=100) index=u/20;
								else if (u<=500) index=(u-100)/100+5;
								else if (u<1000) index=(u-500)/200+9;
								exists = true;
								ArrayList<JSONObject> currWBM = WBMEvaluationResultArray
										.get(index);// result array of u=x
								
								Integer curWBMStaleness = staleResPerWBMEval
										.get(index);// number of stale result
													// for u=x
								for (int l = 0; l < currWBM.size(); l++) {
									exists = true;
									JSONObject tempWBM = currWBM.get(l);
									if(tempWBM!=null){
									Iterator<Var> keyIt = keyvars.iterator();
									while (keyIt.hasNext()) {
										Var tempKey = keyIt.next();
										Object freshValueOfTmpeKeyVar = oneRes
												.get(tempKey.getVarName()
														.toString());
										Object ValueOfTmpeKeyVarInWBM = tempWBM
												.get(tempKey.getVarName()
														.toString());
										if (!freshValueOfTmpeKeyVar
												.equals(ValueOfTmpeKeyVarInWBM)) {
											exists = false;
											break;
										}
									}
									
									if (exists)
										break;
									}
								}
								if (exists = false)
									System.out
											.println("no matching key for csparql result "
													+ oneRes
													+ "in wbm window because window content for this evaluation in csparql and cache were different ... ");
								
								if (currWBM.size()==1&& currWBM.contains(null)) {
									if(u<100)u += 20;
									else if(u<500) u+=100;
									else if (u<1000) u+=200;
									staleResPerWBMEval.set(index,null);
									continue;
								}
								if (!currWBM.contains(oneRes)) {
									curWBMStaleness++;
									staleResPerWBMEval.set(index,
											curWBMStaleness);
								}
								if(u<100)u += 20;
								else if(u<500) u+=100;
								else if (u<1000) u+=200;								
							}
							
	///////////////////////////global
								for (int u =0; u < maxb; ) {// computing freshness for each u
									int index=0;
									if(u<=100) index=(u/20);
									else if (u<=500) index=(u-100)/100+5;
									else if (u<1000) index=(u-500)/200+9;
									exists = true;
									ArrayList<JSONObject> currglobal = globalEvaluationResultArray
											.get(index);// result array of u=x
									
									Integer curglobalStaleness = staleResPerglobalEval
											.get(index);// number of stale result
														// for u=x
									for (int l = 0; l < currglobal.size(); l++) {
										exists = true;
										JSONObject tempglobal = currglobal.get(l);
										if(tempglobal!=null){
										Object freshValueOfTmpeKeyVar = oneRes
													.get("S");
											Object ValueOfTmpeKeyVarInglobal = tempglobal
													.get("S");
											if (!freshValueOfTmpeKeyVar
													.equals(ValueOfTmpeKeyVarInglobal)) {
												exists = false;
												
											}
										
										
										if (exists)
											break;
										}
									}
									if (exists = false)
										System.out
												.println("no matching key for csparql result "
														+ oneRes
														+ "in wbm window because window content for this evaluation in csparql and cache were different ... ");
									
									if (currglobal.size()==1&& currglobal.contains(null)) {
										if(u<100)u += 20;
										else if(u<500) u+=100;
										else if (u<1000) u+=200;
										staleResPerglobalEval.set(index,null);
										continue;
									}
									if (!currglobal.contains(oneRes)) {
										curglobalStaleness++;
										staleResPerglobalEval.set(index,
												curglobalStaleness);
									}
									if(u<100)u += 20;
									else if(u<500) u+=100;
									else if (u<1000) u+=200;								
								}
								
								///////////////////rand
							for (int u = 0; u < maxb;) {// computing freshness for each u
								int index=0;
								if(u<=100) index=u/20;
								else if (u<=500) index=(u-100)/100+5;
								else if (u<1000) index=(u-500)/200+9;
								exists = true;
								ArrayList<JSONObject> currRand = randEvaluationResultArray
										.get(index);// result array of u=x
								
								Integer curRandStaleness = staleResPerrandEval
										.get(index);// number of stale result
														// for u=x
								for (int l = 0; l < currRand.size(); l++) {//per window eval
									exists = true;
									JSONObject tempRand = currRand.get(l);
									if(tempRand!=null){
									Iterator<Var> keyIt = keyvars.iterator();
									while (keyIt.hasNext()) {
										Var tempKey = keyIt.next();
										Object freshValueOfTmpeKeyVar = oneRes
												.get(tempKey.getVarName()
														.toString());
										Object ValueOfTmpeKeyVarInRand = tempRand
												.get(tempKey.getVarName()
														.toString());
										if (!freshValueOfTmpeKeyVar
												.equals(ValueOfTmpeKeyVarInRand)) {
											exists = false;
											break;
										}
									}
									if (exists)
										break;
									}
								}
								if (exists = false)
									System.out
											.println("no matching key for csparql result "
													+ oneRes
													+ "in wbm window because window content for this evaluation in csparql and cache were different ... ");
								
								if (currRand.size()==1&& currRand.contains(null)) {
									if(u<100)u += 20;
									else if(u<500) u+=100;
									else if (u<1000) u+=200;
									staleResPerrandEval.set(index,null);
									continue;
								}if (!currRand.contains(oneRes) ) {
									curRandStaleness++;
									staleResPerrandEval.set(index,
											curRandStaleness);
								}
								if(u<100)u += 20;
								else if(u<500) u+=100;
								else if (u<1000) u+=200;
								
							}
						}
						cacheResultCount = cacheEvaluationResult.size();
						cacheFreshness.write((1 - (double) staleResPerCacheEval
								/ (double) csparqlResultCount)
								+ "\n");
						for (int u = 0; u < maxb;) {
							int index=0;
							if(u<=100) index=u/20;
							else if (u<=500) index=(u-100)/100+5;
							else if (u<1000) index=(u-500)/200+9;
							
							BufferedWriter br = wbmFreshness.get(index);
							try{br.write((1 - (double) staleResPerWBMEval
									.get(index) / (double) csparqlResultCount)
									+ "\n");}catch(Exception e){System.out.println("freshness 1 because update budget is higher than window length");br.write("1\n");}
							if(u<100)u += 20;
							else if(u<500) u+=100;
							else if (u<1000) u+=200;
							
						}
						
						for (int u =0; u < maxb;) {
							int index=0;
							if(u<=100) index=(u/20);
							else if (u<=500) index=(u-100)/100+5;
							else if (u<1000) index=(u-500)/200+9;
							
							BufferedWriter br = globalFreshness.get(index);
							try{br.write((1 - (double) staleResPerglobalEval
									.get(index) / (double) csparqlResultCount)
									+ "\n");}catch(Exception e){System.out.println("freshness 1 because update budget is higher than window length");br.write("1\n");}
							if(u<100)u += 20;
							else if(u<500) u+=100;
							else if (u<1000) u+=200;
							
						}
						
						for (int u = 0; u < maxb; ) {
							int index=0;
							if(u<=100) index=u/20;
							else if (u<=500) index=(u-100)/100+5;
							else if (u<1000) index=(u-500)/200+9;
							
							BufferedWriter br = randFreshness.get(index);
							try{br.write((1 - (double) staleResPerrandEval
									.get(index) / (double) csparqlResultCount)
									+ "\n");}catch(Exception e){System.out.println("freshness 1 because update budget is higher than window length"); br.write("1\n");}
							if(u<100)u += 20;
							else if(u<500) u+=100;
							else if (u<1000) u+=200;
							
						}
					}
					cacheFreshness.flush();
					cacheFreshness.close();
					for (int x = 0; x < 11; x ++) {
						BufferedWriter br = wbmFreshness.get(x);
						br.flush();
						br.close();
						BufferedWriter brr = globalFreshness.get(x);
						brr.flush();
						brr.close();
						BufferedWriter brrr = randFreshness.get(x);
						brrr.flush();
						brrr.close();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
