package eu.larkc.csparql.cep.api;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import org.omg.CORBA.portable.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.sparql.engine.main.iterator.QueryIterService;
import com.hp.hpl.jena.sparql.engine.main.iterator.QueryIterServiceWBM;

import eu.larkc.csparql.common.utils.CsparqlUtils;

public class TestGenFromStreamFile extends RdfStream implements Runnable {

	//private BufferedWriter burnBW;
	private BufferedWriter rw;
	private BufferedWriter sr;
	private String modeOfExec = null;
	protected final Logger logger = LoggerFactory
			.getLogger(TestGenerator.class);
	private BufferedReader streamFile;
	private int c = 2;
	private boolean keepRunning = false;
	private boolean isInit = false;
	// private BufferedWriter bw;

	public TestGenFromStreamFile(final String iri, BufferedWriter bw,
			BufferedWriter statw, String moe) {
		super(iri);
		rw = bw;
		sr = statw;
		modeOfExec = moe;
		
	}

	/*public void run_test() {
		try {
			// BufferedWriter concurr=new BufferedWriter(new FileWriter(new
			// File("d://test.txt")));
			keepRunning = true;
			long startNew = System.currentTimeMillis();
			// TODO shen to change to a fixed timestamp
			startNew = 1438288052584L;
			while (keepRunning) {
				streamFile = new BufferedReader(new FileReader(
						CsparqlUtils.streamFilePath));
				String line = streamFile.readLine();
				String[] strElem = line.split(",");
				long startOld = Long.parseLong(strElem[0]) * 100;
				while (true) {
					ArrayList<RdfQuadruple> concurrentEntries = new ArrayList<RdfQuadruple>();
					final RdfQuadruple q = new RdfQuadruple(
							"http://myexample.org/S"
									+ (Integer.parseInt(strElem[1]) + 1),
							"http://myexample.org/S"
									+ (Integer.parseInt(strElem[1]) + 1),
							"http://myexample.org/S"
									+ (Integer.parseInt(strElem[1]) + 1),
							startNew);
					String timeStamp = strElem[0];
					concurrentEntries.add(q);

					while ((line = streamFile.readLine()) != null) {
						strElem = line.split(",");
						if (strElem[0].equalsIgnoreCase(timeStamp)) {
							concurrentEntries
									.add(new RdfQuadruple(
											"http://myexample.org/S"
													+ (Integer
															.parseInt(strElem[1]) + 1),
											"http://myexample.org/S"
													+ (Integer
															.parseInt(strElem[1]) + 1),
											"http://myexample.org/S"
													+ (Integer
															.parseInt(strElem[1]) + 1),
											startNew));
						} else
							break;
					}
					logger.debug("beark here {}", concurrentEntries);
					for (RdfQuadruple i : concurrentEntries) {
						this.put(i);
						// concurr.write(i.getSubject());
					}
					// concurr.write(startNew+"\n");
					if (line == null)
						break;

					long relativeDifference = Long.parseLong(strElem[0]) * 3000
							- startOld;
					if (relativeDifference < System.currentTimeMillis()
							- startNew)
						throw new RuntimeException(
								"The time required to stream the elements ("
										+ (System.currentTimeMillis() - startNew)
										+ ") is greater than the relative difference ("
										+ relativeDifference + ")");

					System.out.println("--->" + relativeDifference + " ++ "
							+ (System.currentTimeMillis() - startNew));
					if (relativeDifference != 0)
						Thread.sleep(relativeDifference
								- (System.currentTimeMillis() - startNew));

					startOld = Long.parseLong(strElem[0]) * 3000;

					startNew = startNew + relativeDifference;

					// concurr.flush();
				}
				System.out.println("next round of stream file");
			}

		} catch (Exception e) {
			System.out.println("ERROR! stream file cannot be read!!!");
			e.printStackTrace();
		}
	}*/

	public void run() {
		try {
			// BufferedWriter concurr=new BufferedWriter(new FileWriter(new
			// File("d://test.txt")));
			keepRunning = true;
			//long startNew = System.currentTimeMillis();
			// TODO shen to change to a fixed timestamp
			long startNew = CsparqlUtils.startStreamTime;
			long cunrrentStamp = startNew;
			streamFile = new BufferedReader(new FileReader(
						CsparqlUtils.configPath+CsparqlUtils.streamFileName));
				String line = streamFile.readLine();
				String[] strElem = line.split(",");
				// long startOld = Long.parseLong(strElem[0])*100;
				// while (true) {
				ArrayList<RdfQuadruple> concurrentEntries = new ArrayList<RdfQuadruple>();
				cunrrentStamp = startNew + Long.parseLong(strElem[0]);//*100;
				final RdfQuadruple q = new RdfQuadruple(
						"http://myexample.org/S"
								+ (Integer.parseInt(strElem[1]) + 1),
						"http://myexample.org/P"
								+ (Integer.parseInt(strElem[1]) + 1),
						"http://myexample.org/O"
								+ (Integer.parseInt(strElem[1]) + 1),
						cunrrentStamp);
				// String timeStamp= strElem[0];
				concurrentEntries.add(q);
				int counter = 0;
				while (counter < CsparqlUtils.streamLength
						&& (line = streamFile.readLine()) != null) {
					
					counter ++;
					strElem = line.split(",");
					// if(strElem[0].equalsIgnoreCase(timeStamp)){
					// if(tempCounter< batchSize){
					// tempCounter++;
					if (Long.parseLong(strElem[0]) == 0)
						continue;
					cunrrentStamp = startNew + Long.parseLong(strElem[0]);//*100;
					concurrentEntries.add(new RdfQuadruple(
							"http://myexample.org/S"
									+ (Integer.parseInt(strElem[1]) + 1),
							"http://myexample.org/P"
									+ (Integer.parseInt(strElem[1]) + 1),
							"http://myexample.org/O"
									+ (Integer.parseInt(strElem[1]) + 1),
							cunrrentStamp));

					// /}
					// else
					// break;
					
				}
				// logger.debug("beark here {}", concurrentEntries);
				//System.out.println("~~~~~~~~~~"+concurrentEntries.size());
				for (RdfQuadruple i : concurrentEntries) {
					this.put(i);
					// concurr.write(i.getSubject());
				}
				
			//System.out.println("finished sending the stream elements");
			rw.write("]}");
			rw.flush();
			rw.close();
			// sr.write("\n total time : " + getTotalTime());
			sr.write("total call count WBM: " + QueryIterServiceWBM.callCount);
			sr.write("total call count sparql: " + QueryIterService.callCount);
			// sr.write("@@@@@@@@@@@@"+totalWindowEntries);
			sr.flush();
			sr.close();

			
			streamFile.close();
		} catch (Exception e) {
			System.out.println("ERROR! stream file cannot be read!!!");
			e.printStackTrace();
		}
		
		
		Thread.currentThread().interrupt();
	}
	public void read_trace() {
		try {
			keepRunning = true;
			long startNew = CsparqlUtils.startStreamTime;
			long cunrrentStamp = startNew;
			streamFile = new BufferedReader(new FileReader(CsparqlUtils.getStreamTracePath()));
			String line;
			String[] strElem;
			int counter = 0;
			while (counter < CsparqlUtils.streamLength && (line = streamFile.readLine()) != null) {
				counter++;
				strElem = line.split(",");
				if (Long.parseLong(strElem[0]) == 0)
					continue;
				if( !isInit) {
					cunrrentStamp = CsparqlUtils.startStreamTime;
					isInit = true;
				}
				else
					cunrrentStamp = startNew + Long.parseLong(strElem[0]) * 50;
				RdfQuadruple tempQ = new RdfQuadruple("http://myexample.org/S" + (Integer.parseInt(strElem[1]) + 1), "http://myexample.org/P"
						+ (Integer.parseInt(strElem[1]) + 1), "http://myexample.org/O" + (Integer.parseInt(strElem[1]) + 1), cunrrentStamp);
				this.put(tempQ);
			}
			//logger.debug("finish feed entry");
			rw.write("]}");
			rw.flush();
			rw.close();
			sr.flush();
			sr.close();
			streamFile.close();
		} catch (Exception e) {
			System.out.println("ERROR! stream file cannot be read!!!");
			e.printStackTrace();
		}

		
	}
}
