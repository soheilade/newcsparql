/*******************************************************************************
 * Copyright 2014 Davide Barbieri, Emanuele Della Valle, Marco Balduini
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Acknowledgements:
 * 
 * This work was partially supported by the European project LarKC (FP7-215535)
 ******************************************************************************/
package eu.larkc.csparql.cep.api;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.shared.JenaException;
import com.hp.hpl.jena.sparql.engine.main.iterator.QueryIterService;
import com.hp.hpl.jena.sparql.engine.main.iterator.QueryIterServiceWBM;

import eu.larkc.csparql.common.utils.CsparqlUtils;

public class TestGenerator extends RdfStream implements Runnable {

	/** The logger. */
	protected final Logger logger = LoggerFactory
			.getLogger(TestGenerator.class);	
   private int c = 2;
   private boolean keepRunning = false;
   private BufferedWriter bw;
   public TestGenerator(final String iri,BufferedWriter rw) {
      super(iri);
      bw=rw;
      }
   
   public void pleaseStop() {
	   keepRunning = false;
	   try{
		   //bw.write("total time : "+e.getTotalTime());
		   bw.write("call count WBM: "+ QueryIterServiceWBM.callCount);
		   bw.write("call count sparql: "+ QueryIterService.callCount);
		   
	   bw.flush();
	   bw.close();
	   System.out.println("testGenerator");
	   System.exit(0);
	   }catch(Exception e){System.out.println("can't close the result file!!!"); e.printStackTrace();}
	   
   }

   public void run() {
	  keepRunning = true;
	  Random r = new Random(System.currentTimeMillis());
	  //int triplesCount=0;
	  
	  long initTime=System.currentTimeMillis();
	  long click= (long)Math.floor(initTime/500);
	  
	  while (keepRunning) {
		  //triplesCount++;
    	 //long start = System.nanoTime();
    	  long timeLeft=initTime%500;
    	  if(timeLeft==0){
//         final RdfQuadruple q = new RdfQuadruple("http://myexample.org/S" + this.c,
//               "http://myexample.org/P" + this.c, "http://myexample.org/O" + this.c, this.c);
         final RdfQuadruple q = new RdfQuadruple("http://myexample.org/S" + this.c,"http://myexample.org/P" + this.c, "http://myexample.org/O" + this.c, initTime);
         //final RdfQuadruple q = new RdfQuadruple(availableSubjects.get(this.c%33),"http://myexample.org/P" + this.c, "http://myexample.org/O" + this.c, System.currentTimeMillis());
         
         //long end = System.nanoTime();
         //long end = System.currentTimeMillis();
         //long duration = end-start;
//         if(c%10==0) logger.info(c+ " triples streamed so far");
         //if ((duration)>1000) logger.info(((float) c)/((float) duration)*1000000 + " triples/second streamed so far");
         this.put(q);
//         try {
//			Thread.sleep(1000);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
         this.c=(this.c % 32) +1;
         initTime=System.currentTimeMillis();
         click= (long)Math.floor(initTime/500);
    	  }else{
    		  try{Thread.sleep(500-timeLeft);}catch(Exception e){e.printStackTrace();}    
    		  click++;
    		  initTime = click*500;
    		  //System.out.println("~~~~~~~~~~~~~~~~~~~"+initTime+"=="+System.currentTimeMillis());
    	  }
         //if (triplesCount>100) pleaseStop();
      }
   }
}
