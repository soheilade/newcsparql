/*******************************************************************************
 * Copyright 2014 DEIB -Politecnico di Milano
 *   
 *  Marco Balduini (marco.balduini@polimi.it)
 *  Emanuele Della Valle (emanuele.dellavalle@polimi.it)
 *  Davide Barbieri
 *   
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *   
 *  	http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *   
 *  Acknowledgements:
 *  
 *  This work was partially supported by the European project LarKC (FP7-215535)
 ******************************************************************************/
package eu.larkc.csparql.cep.esper;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.concurrent.ArrayBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.espertech.esper.client.Configuration;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.EPStatementState;
import com.espertech.esper.client.soda.StreamSelector;
import com.espertech.esper.client.time.CurrentTimeEvent;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.sparql.engine.acqua.CacheAcqua;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.query.DatasetAccessor;
import com.hp.hpl.jena.query.DatasetAccessorFactory;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.sparql.engine.main.iterator.QueryIterServiceWBM;
import com.hp.hpl.jena.update.UpdateExecutionFactory;
import com.hp.hpl.jena.update.UpdateFactory;
import com.hp.hpl.jena.update.UpdateProcessor;
import com.hp.hpl.jena.update.UpdateRequest;

import eu.larkc.csparql.cep.api.CepEngine;
import eu.larkc.csparql.cep.api.CepQuery;
import eu.larkc.csparql.cep.api.RdfQuadruple;
import eu.larkc.csparql.cep.api.RdfSnapshot;
import eu.larkc.csparql.cep.api.RdfStream;
import eu.larkc.csparql.common.utils.CsparqlUtils;

public class EsperEngine implements CepEngine {

	public static void main(final String[] args){
		try {
			String UQ = "DELETE    { ?a ?b ?c } where{?a ?b ?c}; ";
			UpdateRequest query = UpdateFactory.create(UQ);
			UpdateProcessor qexec = UpdateExecutionFactory.createRemoteForm(
					query, "http://fionn1:3030/test/update");
			qexec.execute();
			String serviceURI = "http://fionn1:3030/test/data";
			DatasetAccessor accessor;
			accessor = DatasetAccessorFactory.createHTTP(serviceURI);
			Model model = ModelFactory.createDefaultModel();
			model.read(new FileInputStream("../acquaCsparql/test.ttl"), null,
					"TTL");
			accessor.putModel(model);
			
			long s= System.currentTimeMillis();
			String u = "http://myexample.org/S35";
			String UQ2 = "DELETE    { <" + u
					+ "> <http://example.org/follower> ?a } where{<" + u
					+ "> <http://example.org/follower> ?a}; INSERT data {<"
					+ u
					+ "> <http://example.org/follower> <http://localhost/10>} ";
			UpdateRequest uquery = UpdateFactory.create(UQ2);
			UpdateProcessor uqexec = UpdateExecutionFactory
					.createRemoteForm(uquery,
							"http://fionn1:3030/test/update");
			uqexec.execute();
	        System.out.println("time taken >>>"+(System.currentTimeMillis()-s));
	        
		} catch (Exception e) {
			e.printStackTrace();
		}
		}
	private EPServiceProvider epService = null;
	private Map<String, CepQuery> queries = null;
	private Collection<RdfStream> streams = null;
	private Map<String, EPStatement> statements = null;
	private final Configuration configuration = new Configuration();
	private ArrayBlockingQueue<RdfQuadruple> queue;
	private boolean enableInjecter;
	public static Logger logger = LoggerFactory.getLogger(EsperEngine.class);
	private Long currentSystemTime;// = 1343860020000L;
	private Long lastSlideTime;// = 1343860020000L;

	private boolean initSysteTime = false;

	public Collection<CepQuery> getAllQueries() {
		return this.queries.values();
	}

	public Collection<RdfStream> getAllRegisteredStreams() {
		return streams;
	}

	public void initialize() {

		// configuration.getEngineDefaults().getThreading().setInternalTimerEnabled(false);

		// Obtain an engine instance
		configuration.getEngineDefaults().getThreading()
				.setInternalTimerEnabled(false);
		configuration.getEngineDefaults().getStreamSelection()
				.setDefaultStreamSelector(StreamSelector.RSTREAM_ISTREAM_BOTH);

		this.epService = EPServiceProviderManager
				.getDefaultProvider(this.configuration);

		// ...and initialize it
		this.epService.initialize();

		// initialize collections
		this.queries = new HashMap<String, CepQuery>();
		this.streams = new ArrayList<RdfStream>();
		this.statements = new HashMap<String, EPStatement>();
	}

	public void setUpInjecter(int queueDimension) {

		if (queueDimension == 0) {
			enableInjecter = false;
		} else {

			enableInjecter = true;
			this.queue = new ArrayBlockingQueue<RdfQuadruple>(queueDimension);

			EsperInjecter esj = new EsperInjecter(queue, this);
			Thread esjThread = new Thread(esj);
			esjThread.start();
		}
	}

	public EPServiceProvider getEpService() {
		return epService;
	}

	public void setEpService(EPServiceProvider epService) {
		this.epService = epService;
	}

	public void registerStream(final RdfStream p) {
		String un = p.uniqueName();
		this.epService.getEPAdministrator().getConfiguration()
				.addImport(RdfQuadruple.class);
		this.epService.getEPAdministrator().getConfiguration()
				.addEventType(un, RdfQuadruple.class);

		// this.epService.getEPAdministrator().getConfiguration()
		// .addEventType(un, CurrentTimeEvent.class);
		p.addObserver(this);
		this.streams.add(p);
	}

	public void unregisterQuery(final String id) {
		this.queries.remove(id);
	}

	public RdfSnapshot registerQuery(final String query, final String id) {

		// String tempQuery = query;
		// tempQuery = tempQuery.replace("time", "time_batch");
		// tempQuery = tempQuery.replace(")",
		// " , \"FORCE_UPDATE , START_EAGER\")");
		// tempQuery = tempQuery.substring(0, tempQuery.indexOf(")"));
		// tempQuery = tempQuery + ") output snapshot every 3 seconds";
		// System.out.println(tempQuery);
		final EsperQuery qry = new EsperQuery(query);
		this.queries.put(id, qry);

		final EPStatement stmt = this.epService.getEPAdministrator().createEPL(
				query);
		this.statements.put(id, stmt);

		final QueryListener l = new QueryListener(id);
		stmt.addListener(l);
		return l;
	}

	public void destroy() {
		this.epService.destroy();
	}

	public void startQuery(final String id) {
		final EPStatement s = this.getStatementById(id);

		if (s != null) {
			s.start();
		}
	}

	public void stopQuery(final String id) {
		final EPStatement s = this.getStatementById(id);

		if (s != null) {
			EPStatementState state = s.getState();
			if (state.compareTo(EPStatementState.STOPPED) != 0)
				s.stop();
		}
	}

	private EPStatement getStatementById(final String id) {
		if (this.statements.containsKey(id)) {
			return this.statements.get(id);
		}

		return null;
	}

	public void unregisterStream(final RdfStream stream) {
		this.streams.remove(stream);
	}

	public String getCepEngineType() {
		return "esper";
	}

	@Override
	public void update(Observable o, Object arg) {

		RdfStream s = (RdfStream) o;
		RdfQuadruple q = (RdfQuadruple) arg;
		q.setStreamName(s.getIRI());

		if (!enableInjecter) {
			this.currentSystemTime = this.setCurrentTimeAndSentEvent(q);

		} else {
			synchronized (queue) {
				try {
					queue.add(q);
				} catch (IllegalStateException e) {
					System.out.println("Queue Full");
				}
			}
		}
		q = null;
		System.gc();

	}

	// TODO shen: this is not very nice
	public Long getSlideLength() {
		for (EPStatement query : this.statements.values()) {
			String[] temp = query.getText().split(" ");
			int i = 0;
			while (i < temp.length) {
				i++;
				if (temp[i].contains("every"))
					break;
			}
			return Long.parseLong(temp[i + 1]);
		}
		return 1L;
	}

	@Override
	public Long getCurrentTime() {
		if (this.currentSystemTime==null ) return CsparqlUtils.startStreamTime;
		return this.currentSystemTime;
	}

	public Long setInitialTime(Long inputTime) {
		long slide = this.getSlideLength() * 1000L;

		if (!this.initSysteTime) {
			// shen : the slide lenght here is hardcoded!
			Long tempTime = inputTime / slide * slide;
			this.currentSystemTime = tempTime;// = 1343860020000L;
			this.lastSlideTime = tempTime;// = 1343860020000L;

			CurrentTimeEvent timeEvent = new CurrentTimeEvent(tempTime);
			this.epService.getEPRuntime().sendEvent(timeEvent);

			this.initSysteTime = true;
			logger.debug("set init Time input {}, actual init time {}",
					inputTime, tempTime);

		}
		return this.currentSystemTime;
	}

	@Override
	public Long setCurrentTimeAndSentEvent(RdfQuadruple q) {

		long inputTime = q.getTimestamp();
		long slide = this.getSlideLength() * 1000L;
		setInitialTime(inputTime);

		while (inputTime >= this.lastSlideTime + slide) {
			this.lastSlideTime += slide;
			this.currentSystemTime = this.lastSlideTime;
			if (inputTime == this.lastSlideTime) {
				//logger.debug(" in setTimeStamp equal input {},  new Time {}",inputTime, this.currentSystemTime);
				this.epService.getEPRuntime().sendEvent(q);
				this.epService.getEPRuntime().sendEvent(
						new CurrentTimeEvent(inputTime));
				return this.currentSystemTime;
			}
			// inputTime > this.lastSlideTime + slide
			CurrentTimeEvent timeEvent = new CurrentTimeEvent(
					this.getCurrentTime());
			this.epService.getEPRuntime().sendEvent(timeEvent);
		}
		if (inputTime > this.currentSystemTime) {
			//logger.debug(" in setTimeStamp last if input {}, new Time {}",inputTime, this.currentSystemTime);
			this.epService.getEPRuntime().sendEvent(
					new CurrentTimeEvent(inputTime));
			this.currentSystemTime = inputTime;
		}
		this.epService.getEPRuntime().sendEvent(q);
		// -------------------------------------------- changing remote data
		/*System.out.println(">>>updating fuseki");
		Iterator<Binding> users = CacheAcqua.INSTANCE.cacheChangeRate.keySet()
				.iterator();
		long t = inputTime ;
		while (users.hasNext()) {
			Binding curUser = users.next();
			long cr = (long) CacheAcqua.INSTANCE.cacheChangeRate.get(curUser);
			String FB = CacheAcqua.INSTANCE.followersBank.get(curUser);			
			if (t % cr == 0) {
				String u = curUser.get(curUser.vars().next()).toString();
				String UQ = "DELETE    { <" + u
						+ "> <http://example.org/follower> ?a } where{<" + u
						+ "> <http://example.org/follower> ?a}; INSERT data {<"
						+ u
						+ "> <http://example.org/follower> <http://localhost/"
						+ FB.split(",")[(int) ((t / cr) % 20)] + ">} ";
				// if ((t-start)/cr>20)
				// {System.out.println("&&&&&&&&&&&&&&&&&&a user has passed more than one round in the list of followers");System.exit(0);}
				UpdateRequest query = UpdateFactory.create(UQ);
				UpdateProcessor qexec = UpdateExecutionFactory
						.createRemoteForm(query,
								"http://localhost:3030/test/update");
				qexec.execute();
			}
		}	*/
		// --------------------------------------------
		return this.currentSystemTime;

	}

}
