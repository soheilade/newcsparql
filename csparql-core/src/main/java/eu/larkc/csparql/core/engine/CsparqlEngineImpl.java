/*******************************************************************************
 * Copyright 2014 DEIB -Politecnico di Milano
 *   
 *  Marco Balduini (marco.balduini@polimi.it)
 *  Emanuele Della Valle (emanuele.dellavalle@polimi.it)
 *  Davide Barbieri
 *   
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *   
 *  	http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *   
 *  Acknowledgements:
 *  
 *  This work was partially supported by the European project LarKC (FP7-215535)
 ******************************************************************************/
package eu.larkc.csparql.core.engine;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import javax.management.RuntimeErrorException;

import org.apache.jena.atlas.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.query.Syntax;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.reasoner.rulesys.GenericRuleReasoner;
import com.hp.hpl.jena.reasoner.rulesys.RDFSRuleReasonerFactory;
import com.hp.hpl.jena.reasoner.rulesys.Rule;
import com.hp.hpl.jena.sparql.core.TriplePath;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.acqua.CacheAcqua;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.engine.main.iterator.QueryIterService;
import com.hp.hpl.jena.sparql.engine.main.iterator.QueryIterServiceCache;
import com.hp.hpl.jena.sparql.engine.main.iterator.QueryIterServiceGlobal;
import com.hp.hpl.jena.sparql.engine.main.iterator.QueryIterServiceRand;
import com.hp.hpl.jena.sparql.engine.main.iterator.QueryIterServiceWBM;
import com.hp.hpl.jena.sparql.syntax.Element;
import com.hp.hpl.jena.sparql.syntax.ElementPathBlock;
import com.hp.hpl.jena.sparql.syntax.ElementService;
import com.hp.hpl.jena.sparql.syntax.ElementVisitorBase;
import com.hp.hpl.jena.sparql.syntax.ElementWalker;
import com.hp.hpl.jena.sparql.util.Symbol;
import com.hp.hpl.jena.update.UpdateExecutionFactory;
import com.hp.hpl.jena.update.UpdateFactory;
import com.hp.hpl.jena.update.UpdateProcessor;
import com.hp.hpl.jena.update.UpdateRequest;
import com.hp.hpl.jena.vocabulary.ReasonerVocabulary;

import eu.larkc.csparql.cep.api.CepEngine;
import eu.larkc.csparql.cep.api.RdfQuadruple;
import eu.larkc.csparql.cep.api.RdfSnapshot;
import eu.larkc.csparql.cep.api.RdfStream;
import eu.larkc.csparql.cep.api.TestGenerator;
import eu.larkc.csparql.cep.esper.EsperEngine;
import eu.larkc.csparql.common.RDFTable;
import eu.larkc.csparql.common.RDFTuple;
import eu.larkc.csparql.common.exceptions.ReasonerException;
import eu.larkc.csparql.common.utils.CsparqlUtils;
import eu.larkc.csparql.common.utils.ReasonerChainingType;
import eu.larkc.csparql.core.Configuration;
import eu.larkc.csparql.core.new_parser.utility_files.LogicalWindow;
import eu.larkc.csparql.core.new_parser.utility_files.StreamInfo;
import eu.larkc.csparql.core.new_parser.utility_files.TimeUtils;
import eu.larkc.csparql.core.new_parser.utility_files.Translator;
import eu.larkc.csparql.core.streams.formats.CSparqlQuery;
import eu.larkc.csparql.core.streams.formats.TranslationException;
import eu.larkc.csparql.sparql.api.SparqlEngine;
import eu.larkc.csparql.sparql.api.SparqlQuery;
import eu.larkc.csparql.sparql.jena.JenaEngine;

public class CsparqlEngineImpl implements Observer, CsparqlEngine {

	private Configuration configuration = null;
	private Collection<CSparqlQuery> queries = null;
	private Map<String, RdfStream> streams = null;
	private Map<CSparqlQuery, RdfSnapshot> snapshots = null;
	private Map<CSparqlQuery, CsparqlQueryResultProxy> results = null;
	private CepEngine cepEngine = null;
	private SparqlEngine sparqlEngine = null;
	private Reasoner reasoner = null;
	private BufferedWriter rw;
	private BufferedWriter stats;
	private String MOE;
	private int exeTime = 1;
	public static int totalWindowEntries = 0;
	private int update =0;
	
	public CsparqlEngineImpl(BufferedWriter bw,BufferedWriter statw,String moe) {
		rw=bw;
		stats=statw;		
		MOE = moe;
		totalWindowEntries=0;
		update=0;
	}
	

	protected final Logger logger = LoggerFactory
			.getLogger(CsparqlEngineImpl.class);

	public Collection<CSparqlQuery> getAllQueries() {
		return this.queries;
	}

	public void initialize() {
		this.configuration = Configuration.getCurrentConfiguration();
		this.queries = new ArrayList<CSparqlQuery>();
		this.streams = new HashMap<String, RdfStream>();
		// this.firstEval=new HashMap<CSparqlQuery,Boolean>();
		this.snapshots = new HashMap<CSparqlQuery, RdfSnapshot>();
		this.results = new HashMap<CSparqlQuery, CsparqlQueryResultProxy>();
		this.sparqlEngine = this.configuration.createSparqlEngine( );
		this.cepEngine = this.configuration.createCepEngine();
		this.reasoner = this.configuration.createReasoner();
		this.cepEngine.initialize();
		this.sparqlEngine.initialize();
		this.setPerformTimestampFunctionVariable(false);
		this.setUpInjecter(0);
	}

	public void initialize(int queueDimension) {
		this.configuration = Configuration.getCurrentConfiguration();
		this.queries = new ArrayList<CSparqlQuery>();
		this.streams = new HashMap<String, RdfStream>();
		// this.firstEval=new HashMap<CSparqlQuery,Boolean>();
		this.snapshots = new HashMap<CSparqlQuery, RdfSnapshot>();
		this.results = new HashMap<CSparqlQuery, CsparqlQueryResultProxy>();
		this.sparqlEngine = this.configuration.createSparqlEngine();
		this.cepEngine = this.configuration.createCepEngine();
		this.reasoner = this.configuration.createReasoner();
		this.cepEngine.initialize();
		this.sparqlEngine.initialize();
		this.setPerformTimestampFunctionVariable(false);
		this.setUpInjecter(queueDimension);
	}

	public void initialize(boolean performTimestampFunction) {
		this.configuration = Configuration.getCurrentConfiguration();
		this.queries = new ArrayList<CSparqlQuery>();
		this.streams = new HashMap<String, RdfStream>();
		// this.firstEval=new HashMap<CSparqlQuery,Boolean>();
		this.snapshots = new HashMap<CSparqlQuery, RdfSnapshot>();
		this.results = new HashMap<CSparqlQuery, CsparqlQueryResultProxy>();
		this.sparqlEngine = this.configuration.createSparqlEngine();
		this.cepEngine = this.configuration.createCepEngine();
		this.reasoner = this.configuration.createReasoner();
		this.cepEngine.initialize();
		this.sparqlEngine.initialize();
		this.setPerformTimestampFunctionVariable(performTimestampFunction);
		this.setUpInjecter(0);
	}

	public void initialize(int queueDimension, boolean performTimestampFunction) {
		this.configuration = Configuration.getCurrentConfiguration();
		this.queries = new ArrayList<CSparqlQuery>();
		this.streams = new HashMap<String, RdfStream>();
		// this.firstEval=new HashMap<CSparqlQuery,Boolean>();
		this.snapshots = new HashMap<CSparqlQuery, RdfSnapshot>();
		this.results = new HashMap<CSparqlQuery, CsparqlQueryResultProxy>();
		this.sparqlEngine = this.configuration.createSparqlEngine();
		this.cepEngine = this.configuration.createCepEngine();
		this.reasoner = this.configuration.createReasoner();
		this.cepEngine.initialize();
		this.sparqlEngine.initialize();
		this.setPerformTimestampFunctionVariable(performTimestampFunction);
		this.setUpInjecter(queueDimension);
	}

	public void setPerformTimestampFunctionVariable(boolean value) {
		if (sparqlEngine.getEngineType().equals("jena")) {
			JenaEngine je = (JenaEngine) sparqlEngine;
			je.setPerformTimestampFunctionVariable(value);
		}
	}

	public void setUpInjecter(int queueDimension) {
		if (cepEngine.getCepEngineType().equals("esper")) {
			EsperEngine ee = (EsperEngine) cepEngine;
			ee.setUpInjecter(queueDimension);
		}
	}

	// @Override
	// public void activateInference() {
	// sparqlEngine.activateInference();
	// }
	//
	// @Override
	// public void activateInference(String rulesFile, String
	// entailmentRegimeType) {
	// sparqlEngine.activateInference(rulesFile, entailmentRegimeType);
	// }
	//
	// @Override
	// public void activateInference(String rulesFile, String
	// entailmentRegimeType, String tBoxFile) {
	// sparqlEngine.activateInference(rulesFile, entailmentRegimeType,
	// tBoxFile);
	// }

	@Override
	public boolean getInferenceStatus() {
		return sparqlEngine.getInferenceStatus();
	}

	@Override
	public void arrestInference(String queryId) {
		try {
			sparqlEngine.arrestInference(queryId);
		} catch (ReasonerException e) {
			logger.error(e.getMessage(), e);
		}
	}

	@Override
	public void restartInference(String queryId) {
		try {
			sparqlEngine.restartInference(queryId);
		} catch (ReasonerException e) {
			logger.error(e.getMessage(), e);
		}
	}

	@Override
	public void updateReasoner(String queryId) {
		sparqlEngine.updateReasoner(queryId);
	}

	@Override
	public void updateReasoner(String queryId, String rulesFile,
			ReasonerChainingType chainingType) {
		sparqlEngine.updateReasoner(queryId, rulesFile, chainingType);
	}

	@Override
	public void updateReasoner(String queryId, String rulesFile,
			ReasonerChainingType chainingType, String tBoxFile) {
		sparqlEngine.updateReasoner(queryId, rulesFile, chainingType, tBoxFile);
	}

	@Override
	public void execUpdateQueryOverDatasource(String queryBody) {
		sparqlEngine.execUpdateQueryOverDatasource(queryBody);
	}

	@Override
	public void putStaticNamedModel(String iri, String serialization) {
		sparqlEngine.putStaticNamedModel(iri, serialization);
	}

	@Override
	public void removeStaticNamedModel(String iri) {
		sparqlEngine.removeStaticNamedModel(iri);
	}

	private CSparqlQuery getQueryByID(final String id) {
		for (final CSparqlQuery q : this.queries) {
			if (q.getId().equalsIgnoreCase(id)) {
				return q;
			}
		}

		return null;
	}

	public RdfStream registerStream(final RdfStream s) {
		this.streams.put(s.getIRI(), s);
		this.cepEngine.registerStream(s);
		return s;
	}

	public void unregisterDataProvider(final RdfStream provider) {
		this.streams.remove(provider);
	}

	private void unregisterAllQueries() {

		for (final CSparqlQuery q : this.queries) {
			this.unregisterQuery(q.getId());
		}
	}

	public void startQuery(final String id) {
		this.cepEngine.startQuery(id);
	}

	public void stopQuery(final String id) {

		this.cepEngine.stopQuery(id);
	}

	private void unregisterQuery(final CSparqlQuery q) {

		this.stopQuery(q.getId());

		if (q != null) {
			this.queries.remove(q);
		}
	}

	public void unregisterQuery(final String id) {
		final CSparqlQuery q = this.getQueryByID(id);
		this.unregisterQuery(q);
	}

	public void unregisterStream(final String iri) {

		final RdfStream r = this.getStreamByIri(iri);

		if (r == null) {
			return;
		}

		this.streams.remove(iri);
	}

	@Override
	public CsparqlQueryResultProxy registerQuery(String command,
			boolean activateInference) throws ParseException {

		final Translator t = Configuration.getCurrentConfiguration()
				.createTranslator(this);

		CSparqlQuery query = null;

		// extract shared variables and cache initialization

		// Split continuous part from static part
		try {
			query = t.translate(command);
		} catch (final TranslationException e) {
			throw new ParseException(e.getMessage(), 0);
		} catch (Exception e) {
			e.printStackTrace();
		}

		logger.debug("CEP query: {}", query.getCepQuery().getQueryCommand());
		logger.debug("SPARQL query: {}", query.getSparqlQuery()
				.getQueryCommand().replace("\n", " ").replace("\r", " "));

		// Parse sparql(static) query
		
		sparqlEngine.parseSparqlQuery(query.getSparqlQuery(), MOE,this.cepEngine.getCurrentTime());
		
		// run the stream generator after the cache initialoization
		final RdfSnapshot s = this.cepEngine.registerQuery(query.getCepQuery()
				.getQueryCommand(), query.getId());

		final CsparqlQueryResultProxy result = new CsparqlQueryResultProxy(
				query.getId());
		result.setSparqlQueryId(query.getSparqlQuery().getId());
		result.setCepQueryId(query.getCepQuery().getId());

		this.queries.add(query);
		this.snapshots.put(query, s);
		this.results.put(query, result);
		// this.firstEval.put(query, true);
		s.addObserver(this);

		if (activateInference) {
			logger.debug("RDFS reasoner");
			Resource config = ModelFactory.createDefaultModel()
					.createResource()
					.addProperty(ReasonerVocabulary.PROPsetRDFSLevel, "simple");
			com.hp.hpl.jena.reasoner.Reasoner reasoner = RDFSRuleReasonerFactory
					.theInstance().create(config);
			sparqlEngine.addReasonerToReasonerMap(query.getSparqlQuery()
					.getId(), reasoner);
		}

		return result;
	}

	@Override
	public CsparqlQueryResultProxy registerQuery(String command,
			boolean activateInference, String rulesFileSerialization,
			ReasonerChainingType chainingType) throws ParseException {

		final Translator t = Configuration.getCurrentConfiguration()
				.createTranslator(this);

		CSparqlQuery query = null;

		// Split continuous part from static part
		try {
			query = t.translate(command);
		} catch (final TranslationException e) {
			throw new ParseException(e.getMessage(), 0);
		} catch (Exception e) {
			e.printStackTrace();
		}

		logger.debug("CEP query: {}", query.getCepQuery().getQueryCommand());
		logger.debug("SPARQL query: {}", query.getSparqlQuery()
				.getQueryCommand().replace("\n", "").replace("\r", ""));

		// Parse sparql(static) query
		sparqlEngine.parseSparqlQuery(query.getSparqlQuery(), MOE,this.cepEngine.getCurrentTime());
		final RdfSnapshot s = this.cepEngine.registerQuery(query.getCepQuery()
				.getQueryCommand(), query.getId());

		final CsparqlQueryResultProxy result = new CsparqlQueryResultProxy(
				query.getId());
		result.setSparqlQueryId(query.getSparqlQuery().getId());
		result.setCepQueryId(query.getCepQuery().getId());

		this.queries.add(query);
		this.snapshots.put(query, s);
		this.results.put(query, result);
		// this.firstEval.put(query, true);

		s.addObserver(this);

		if (activateInference) {
			logger.debug("Generic Rule Engine");
			com.hp.hpl.jena.reasoner.Reasoner reasoner = new GenericRuleReasoner(
					Rule.parseRules(Rule
							.rulesParserFromReader(new BufferedReader(
									new StringReader(rulesFileSerialization)))));
			switch (chainingType) {
			case BACKWARD:
				reasoner.setParameter(ReasonerVocabulary.PROPruleMode,
						"backward");
				break;
			case FORWARD:
				reasoner.setParameter(ReasonerVocabulary.PROPruleMode,
						"forward");
				break;
			case HYBRID:
				reasoner.setParameter(ReasonerVocabulary.PROPruleMode, "hybrid");
				break;
			default:
				reasoner.setParameter(ReasonerVocabulary.PROPruleMode,
						"forward");
				break;
			}
			sparqlEngine.addReasonerToReasonerMap(query.getSparqlQuery()
					.getId(), reasoner);
		}

		return result;

	}

	@Override
	public CsparqlQueryResultProxy registerQuery(String command,
			boolean activateInference, String rulesFileSerialization,
			ReasonerChainingType chainingType, String tBoxFileSerialization)
			throws ParseException {

		final Translator t = Configuration.getCurrentConfiguration()
				.createTranslator(this);

		CSparqlQuery query = null;

		// Split continuous part from static part
		try {
			query = t.translate(command);
		} catch (final TranslationException e) {
			throw new ParseException(e.getMessage(), 0);
		} catch (Exception e) {
			e.printStackTrace();
		}

		logger.debug("CEP query: {}", query.getCepQuery().getQueryCommand());
		logger.debug("SPARQL query: {}", query.getSparqlQuery()
				.getQueryCommand().replace("\n", "").replace("\r", ""));

		// Parse sparql(static) query
		sparqlEngine.parseSparqlQuery(query.getSparqlQuery(), MOE,this.cepEngine.getCurrentTime());

		final RdfSnapshot s = this.cepEngine.registerQuery(query.getCepQuery()
				.getQueryCommand(), query.getId());

		final CsparqlQueryResultProxy result = new CsparqlQueryResultProxy(
				query.getId());
		result.setSparqlQueryId(query.getSparqlQuery().getId());
		result.setCepQueryId(query.getCepQuery().getId());

		this.queries.add(query);
		this.snapshots.put(query, s);
		this.results.put(query, result);
		// this.firstEval.put(query, true);

		s.addObserver(this);

		if (activateInference) {
			logger.debug("Generic Rule Engine");
			com.hp.hpl.jena.reasoner.Reasoner reasoner = new GenericRuleReasoner(
					Rule.parseRules(Rule
							.rulesParserFromReader(new BufferedReader(
									new StringReader(rulesFileSerialization)))));
			switch (chainingType) {
			case BACKWARD:
				reasoner.setParameter(ReasonerVocabulary.PROPruleMode,
						"backward");
				break;
			case FORWARD:
				reasoner.setParameter(ReasonerVocabulary.PROPruleMode,
						"forward");
				break;
			case HYBRID:
				reasoner.setParameter(ReasonerVocabulary.PROPruleMode, "hybrid");
				break;
			default:
				reasoner.setParameter(ReasonerVocabulary.PROPruleMode,
						"forward");
				break;
			}
			try {
				reasoner = reasoner.bindSchema(ModelFactory
						.createDefaultModel().read(
								new StringReader(tBoxFileSerialization), null,
								"RDF/XML"));
			} catch (Exception e) {
				try {
					reasoner = reasoner.bindSchema(ModelFactory
							.createDefaultModel().read(
									new StringReader(tBoxFileSerialization),
									null, "N-TRIPLE"));
				} catch (Exception e1) {
					try {
						reasoner = reasoner.bindSchema(ModelFactory
								.createDefaultModel()
								.read(new StringReader(tBoxFileSerialization),
										null, "TURTLE"));
					} catch (Exception e2) {
						try {
							reasoner = reasoner.bindSchema(ModelFactory
									.createDefaultModel().read(
											new StringReader(
													tBoxFileSerialization),
											null, "RDF/JSON"));
						} catch (Exception e3) {
							logger.error(e.getMessage(), e3);
						}
					}
				}
			}
			sparqlEngine.addReasonerToReasonerMap(query.getSparqlQuery()
					.getId(), reasoner);
		}

		return result;
	}

	public void destroy() {

		this.unregisterAllQueries();
		this.cepEngine.destroy();
	}

	// Snapshot received
	// public void update(final GenericObservable<List<RdfQuadruple>> observed,
	// final List<RdfQuadruple> quads) {
	//
	// long starttime = System.nanoTime();
	//
	// final RdfSnapshot r = (RdfSnapshot) observed;
	//
	// final CSparqlQuery csparqlquery = this.getQueryByID(r.getId());
	//
	// final RdfSnapshot augmentedSnapshot = this.reasoner.augment(r);
	//
	// this.snapshots.put(csparqlquery, augmentedSnapshot);
	//
	// this.sparqlEngine.clean();
	//
	// long count = 0;
	//
	// for (final RdfQuadruple q : quads) {
	// if (isStreamUsedInQuery(csparqlquery, q.getStreamName()))
	// {
	// this.sparqlEngine.addStatement(q.getSubject(), q.getPredicate(),
	// q.getObject(), q.getTimestamp());
	// count++;
	// }
	// }
	//
	// if (count == 0)
	// return;
	//
	// final RDFTable result =
	// this.sparqlEngine.evaluateQuery(csparqlquery.getSparqlQuery());
	//
	// timestamp(result, csparqlquery);
	//
	// logger.info("results obtained in "+ (System.nanoTime()-starttime) +
	// " nanoseconds");
	//
	// this.notifySubscribers(csparqlquery, result);
	//
	//
	// }

	// private void timestamp(RDFTable r, CSparqlQuery q) {
	// if (q.getQueryCommand().toLowerCase().contains("register stream"))
	// r.add("timestamp", "0");
	// //TODO: da aggiungere il campo on the fly
	//
	// }

	private boolean isStreamUsedInQuery(CSparqlQuery csparqlquery,
			String streamName) {
		for (StreamInfo si : csparqlquery.getStreams()) {
			if (si.getIri().equalsIgnoreCase(streamName))
				return true;
		}

		return false;
	}

	private void notifySubscribers(final CSparqlQuery csparqlquery,
			final RDFTable result) {

		final CsparqlQueryResultProxy res = this.results.get(csparqlquery);

		res.notify(result);
	}

	public long getTotalTime() {
		return ((JenaEngine) (this.sparqlEngine)).totalTime;
	}

	public double getTotalMem() {
		return ((JenaEngine) (this.sparqlEngine)).totalMemory;
	}

	public RdfStream getStreamByIri(final String iri) {

		if (this.streams.containsKey(iri)) {
			return this.streams.get(iri);
		}

		return null;
	}
	
	/*public void updateServer2(long curTime){
		String UQ="";
		
		ArrayList<Integer> finalValues=new ArrayList<Integer>();//an array list that keeps the final values of each entry to update
		Iterator<Binding> users = CacheAcqua.INSTANCE.cacheChangeRate.keySet().iterator();
		while(users.hasNext()){
			finalValues.add(-1);
			users.next();
			//System.out.println(users.next()+" "+finalValues.size());
		}
		long min=Long.MAX_VALUE;long max=Long.MIN_VALUE;
		//System.out.println("out of initialization");
		for (final RdfQuadruple q : quads) {
			long t = q.getTimestamp();
				if(t<min) min=t;
				if(t>max) max=t;
				//-----------------------
				count++;
			}
		
		//--------------
		int uid=0;
		users = CacheAcqua.INSTANCE.cacheChangeRate.keySet().iterator();
		while(users.hasNext()){			
			Binding curUser = users.next();
			long cr = (long) CacheAcqua.INSTANCE.cacheChangeRate.get(curUser);
			String FB = CacheAcqua.INSTANCE.followersBank.get(curUser);	
			String u = curUser.get(curUser.vars().next()).toString();			
			for(long x=min ; x<max;x++){
			if (x % cr == 0) {
				finalValues.set(uid, Integer.parseInt(FB.split(",")[(int) ((x / cr) % 20)]));				
			}			
		}
		uid++;
		}
		int updatedusercount=0;
		while(users.hasNext()){
			Binding curUser = users.next();
			if(finalValues.get(uid)!=-1){
				updatedusercount++;
			String u = curUser.get(curUser.vars().next()).toString();			
			UQ += "DELETE    { <" + u
					+ "> <http://example.org/follower> ?a } where{<" + u
					+ "> <http://example.org/follower> ?a}; INSERT data {<"
					+ u
					+ "> <http://example.org/follower> <http://localhost/"
					+ finalValues.get(uid) + ">}; ";
			}
			uid++;
		}
		UpdateRequest query = UpdateFactory.create(UQ);
		UpdateProcessor qexec = UpdateExecutionFactory.createRemoteForm(query,
						"http://localhost:3030/test/update");
		qexec.execute();
		//System.out.println("updatedusercount"+updatedusercount);
		try{Thread.sleep(300);}catch(Exception e){e.printStackTrace();}
		
	}*/

	public void updateServer(long currentTime,String step){
		//System.out.println("server updater for "+currentTime+"step"+step);
		Iterator<Binding> users = CacheAcqua.INSTANCE.cacheChangeRate.keySet().iterator();
		
				int totalTripple = 0;
				while (users.hasNext()) {
					Binding tempBKGSR = users.next();
					long changeRate = (long) CacheAcqua.INSTANCE.cacheChangeRate.get(tempBKGSR);
					long lastUpdateTime = CacheAcqua.INSTANCE.lastUpdateTimeInRemote.get(tempBKGSR);
					String FB = CacheAcqua.INSTANCE.followersBank.get(tempBKGSR);
					if ((currentTime - lastUpdateTime) >= changeRate) {
						String u = tempBKGSR.get(tempBKGSR.vars().next()).toString();
						// delete first SR that is expired
						String UQ = "DELETE    { <" + u
								+ "> <http://example.org/follower> ?a } where{<" + u
								+ "> <http://example.org/follower> ?a}";
		
						int index=(int) ((currentTime / (Long.parseLong(step)*1000)) % 100);
						String insertStatment = "INSERT data {<"
								+ u
								+ "> <http://example.org/follower> <http://localhost/"
								+FB.split(",")[index] + "> . }; ";
						
						/*if(tempBKGSR.get(Var.alloc("S")).toString().contains("S16"))
							{System.out.println("curtime->"+currentTime+" lastupdatetime->"+lastUpdateTime+" changeRate->"+changeRate+" new value->"+FB.split(",")[index]);
							System.out.println(">>>>>>>>"+index);
							
							}*/
						String onverallStatment = UQ + "; " + insertStatment;
		
						UpdateRequest query = UpdateFactory.create(onverallStatment);
						UpdateProcessor qexec = UpdateExecutionFactory.createRemoteForm(query, "http://localhost:3030/test/update");
						qexec.execute();
		
						
						CacheAcqua.INSTANCE.lastUpdateTimeInRemote.put(tempBKGSR, (currentTime / changeRate) * changeRate);
					}
				}
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public void update(Observable o, Object arg) {
		long st = System.currentTimeMillis();
		// long starttime = System.nanoTime();
		final RdfSnapshot r = (RdfSnapshot) o;
		List<RdfQuadruple> quads = (List<RdfQuadruple>) arg;
		//System.out.println("~~~~~~~~~~~update"+quads.size());
		
		final CSparqlQuery csparqlquery = this.getQueryByID(r.getId());

		final RdfSnapshot augmentedSnapshot = this.reasoner.augment(r);

		this.snapshots.put(csparqlquery, augmentedSnapshot);

		this.sparqlEngine.clean();

		long count = 0;

		ArrayList<String> windowContent = new ArrayList<>();

		for (final RdfQuadruple q : quads) {
			if (isStreamUsedInQuery(csparqlquery, q.getStreamName())) {
				windowContent.add(q.getSubject() + ">>>" + q.getTimestamp());
				((JenaEngine) this.sparqlEngine)
						.setPerformTimestampFunctionVariable(true);
				// System.out.println(q.getSubject()+","+ q.getPredicate()+","+
				// q.getObject()+","+ q.getTimestamp());
				this.sparqlEngine.addStatement(q.getSubject(),
						q.getPredicate(), q.getObject(), q.getTimestamp());
				count++;
				totalWindowEntries++;
			}
		}
		
		//------------------------
		Iterator<Binding> users = CacheAcqua.INSTANCE.cacheChangeRate.keySet().iterator();
		/*if(sparqlEngine.getModelSize()<update) {
			//these static variables will be set in the evaluate query function if update is higher than size of window in next
			//round and if it is lower than size of window then it should be zero anyways because it will have the same time as 
			//csparql and we don't want to re-evaluate it
			 		((JenaEngine) (this.sparqlEngine)).totalTime =0;
					((JenaEngine) (this.sparqlEngine)).timeConstructor =0;
					((JenaEngine) (this.sparqlEngine)).timeNextStage=0;
					((JenaEngine) (this.sparqlEngine)).totalMemory =0;
					QueryIterServiceRand.callCount =0;
					QueryIterServiceWBM.callCount =0;
					QueryIterService.callCount =0;
			try{stats.write(windowContent.size() + ","
					+ ((JenaEngine) (this.sparqlEngine)).totalTime + ","//th
					+ ((JenaEngine) (this.sparqlEngine)).timeConstructor + ","
					+ ((JenaEngine) (this.sparqlEngine)).timeNextStage+ ","
					+ ((JenaEngine) (this.sparqlEngine)).totalMemory + ","
					+ QueryIterServiceRand.callCount + ","
					+ QueryIterServiceWBM.callCount + ","
					+ QueryIterService.callCount + ",0,"
					+ windowContent + "," + (System.currentTimeMillis() - st)
					+ "\n");
			RDFTable nul=new RDFTable();
			nul.setJsonSerialization("{  \"head\": {    \"vars\": [ \"S\" , \"P\" , \"O\" , \"P2\" , \"O2\" ]  } ,  \"results\": {    \"bindings\": []}}");
			if (exeTime == 1)
				{rw.write("{\"responses\":[\n"); exeTime++;}
			rw.write(nul.getJsonSerialization());
			
			}catch(Exception e){e.printStackTrace();}
			return;
		}*/
		
		if (count == 0)
			return;

		
		// final RDFTable result =
		// this.sparqlEngine.evaluateQuery(csparqlquery.getSparqlQuery());
		ArrayList<StreamInfo> sss = new ArrayList<StreamInfo>(
				csparqlquery.getStreams());
		LogicalWindow l = (LogicalWindow) sss.get(0).getWindow();
		String rangeStr = TimeUtils.getSeconds(l.getRangeDescription()
				.getValue(), l.getRangeDescription().getTimeUnit());
		String slideStr = TimeUtils.getSeconds(l.getStepDescription()
				.getValue(), l.getStepDescription().getTimeUnit());
		final String range = rangeStr.substring(0,
				rangeStr.indexOf("seconds") - 1).trim();
		final String step = slideStr.substring(0,
				slideStr.indexOf("seconds") - 1).trim();
		
		updateServer(this.cepEngine.getCurrentTime(),step);
		try{Thread.sleep(1000);}catch(Exception e){e.printStackTrace();}
		// logger.debug(">>>> range {} step {} ",range,step);
		final RDFTable result = ((JenaEngine) this.sparqlEngine).evaluateQuery(
				csparqlquery.getSparqlQuery(), range, step,this.cepEngine.getCurrentTime());
		// timestamp(result, csparqlquery);

		// logger.info("results obtained in "+ (System.nanoTime()-starttime) +
		// " nanoseconds");
		try {if (exeTime == 1)
			rw.write("{\"responses\":[\n");

		this.notifySubscribers(csparqlquery, result);

		rw.write(result.getJsonSerialization());
		//if(MOE.equalsIgnoreCase("csparql")) CsparqlUtils.lengthOfWindows.add(result.size());
		long timecons=0;
		long timeNext=0;
		if(MOE.equalsIgnoreCase("WBM"))
			{
				timecons=QueryIterServiceWBM.totalTimeCons;
				timeNext=QueryIterServiceWBM.totalTimeNext;
			}
		else if (MOE.equalsIgnoreCase("rand")){
			timecons=QueryIterServiceRand.totalTimeCons;
			timeNext=QueryIterServiceRand.totalTimeNext;
		}else if (MOE.equalsIgnoreCase("cache")){
			timecons=QueryIterServiceCache.totalTimeCons;
			timeNext=QueryIterServiceCache.totalTimeNext;
		}else if (MOE.equalsIgnoreCase("csparql")){
			timecons=QueryIterService.totalTimeCons;
			timeNext=QueryIterService.totalTimeNext;
		}else if (MOE.equalsIgnoreCase("global")){
			timecons=QueryIterServiceGlobal.totalTimeCons;
			timeNext=QueryIterServiceGlobal.totalTimeNext;
		}
		stats.write(windowContent.size() + ","
				+ (timecons+timeNext)+ ","
				+ timecons + ","
				+ timeNext+ ","
				/*
				+ ((JenaEngine) (this.sparqlEngine)).totalTime + ","
				+ ((JenaEngine) (this.sparqlEngine)).timeConstructor + ","
				+ ((JenaEngine) (this.sparqlEngine)).timeNextStage+ "," */
				+ ((JenaEngine) (this.sparqlEngine)).totalMemory + ","
				+ QueryIterServiceRand.callCount + ","
				+ QueryIterServiceWBM.callCount + ","
				+ QueryIterService.callCount + "," + result.size() + ","
				+ windowContent + "," + (System.currentTimeMillis() - st)
				+ "\n");
		//stats.flush();
		/*logger.debug(
				"windowSize {} , totaltime {} , totalMem {} , wbmCallCount {} , CScallCount {}, resultSize {} , windowContetnt {}, timetakenForCurEval {} ",
				windowContent.size(),
				((JenaEngine) (this.sparqlEngine)).totalTime,
				((JenaEngine) (this.sparqlEngine)).totalMemory,
				QueryIterServiceWBM.callCount, QueryIterService.callCount,
				result.size(), windowContent,
				(System.currentTimeMillis() - st));*/
		
		
		exeTime++;
		} catch (IOException e) {
			System.out.println("can't write the result!!!");
			e.printStackTrace();
		}
	}
		
	
	@Override
	public void setUpdateBudget(int u) {
		update=u;
		sparqlEngine.setUpdateBudget(u);		
	}

	@Override
	public void setMOE(String modeOfExecution) {
		this.sparqlEngine.setMOE(modeOfExecution);
		
	}
	
}
